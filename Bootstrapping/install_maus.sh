#! /bin/bash

# installs into ./maus
# creates temporary files in current working directory

# installs maus

# dependencies
# maus depends on awk but everyone should have that installed so I don't even test.
# test that awk is actually gawk not mawk
if ! awk --version 2>&1 | grep -q "GNU Awk"; then
    echo "Your awk does not seem to be GNU Awk, which is required for maus. Exiting." 
    exit
else
    echo "awk is GNU Awk, that's good."
fi
if [ ! -f /bin/csh ]; then
    echo "Maus requires csh to be installed."
fi

maus_dir=./maus/
maus_path="ftp://ftp.phonetik.uni-muenchen.de/pub/BAS/SOFTW/MAUS/"
maus_file="maus-1611101528.tgz" # versions after this one are 10 times as large to download!
if [ ! -d $maus_dir ]; then
    mkdir $maus_dir
    cd $maus_dir
    if [ ! -f $maus_file ]; then
        wget "$maus_path/$maus_file"
    fi
    tar -xf $maus_file # unpacks into current directory
    maus_fulldir=`pwd`
    # replace Florian Schiel's install path with ours
    sed -i "s#SOURCE = /homes/schiel/MAUS/TOOL#SOURCE = $maus_fulldir#" maus mausbpf2emuR mausbpfDB2emuRDB maus.corpus maus.iter maus.trn par2emu par2TextGrid
    cd -
fi
exit

echo "Installation finished."
