#! /usr/bin/env python3
import argparse
import logging
import os
import os.path
import random
import re
from shutil import copyfile
import subprocess
import sys

parser = argparse.ArgumentParser(description="""

This script can be used to generate various files needed for
sphinxtrain (gen).  It will take into account already existing files.
Depending on which files are given as input, all outputfiles which are
possible to generate will be generated.  Also the sphinx_train.cfg
file can be modified (cfg).

For detailed information about individual steps please have a look at
the documenation in the source code.

""")


parser.add_argument("-v", "--verbose",
                    help="increase verbosity once",
                    action="count", default=1)


subparsers = parser.add_subparsers(help="sub command to execute",
                                   dest="sub_command_name")

parser_cfg = subparsers.add_parser("cfg", help="modify the config file")

parser_cfg.add_argument("cfg_file",
                        nargs="?", default="etc/sphinx_train.cfg",
                        help="the config file usually located in etc/sphinx_train.cfg")

parser_gen = subparsers.add_parser("gen", help="generate all the files")

parser_gen.add_argument("-p", "--prefix",
                        help="directory prefix to task directory")
parser_gen.add_argument("-T", "--no_test",
                        help="prevents splitting of data into test and train",
                        action="store_true")
parser_gen.add_argument("-c", "--caps",
                        help="convert all text to ALL-CAPS",
                        action="store_true")
parser_gen.add_argument("g2p_bin",
                        help="the path to the binary for generating a g2p model")
parser_gen.add_argument("g2p_model",
                        help="the model required by sequitur")
parser_gen.add_argument("name",
                        help="the name of the task as it is used in sphinxtrain")
parser_gen.add_argument("wav_dir",
                        nargs="?", default="wav",
                        help="the path to the wav directory containing the samples")
parser_gen.add_argument("split",
                        nargs="?", type=float, default=0.1,
                        help="the portion used for testing, default is 0.1")

FILENAMES = {"snippetsdir":         "wav",
             "fileids":             "etc/misc.{name}.fileids",
             "test_fileids":        "etc/{name}_test.fileids",
             "train_fileids":       "etc/{name}_train.fileids",
             "removed_fileids":     "etc/{name}_removed.fileids",
             "transcription":       "etc/misc.{name}.transcription",
             "test_transcription":  "etc/{name}_test.transcription",
             "train_transcription": "etc/{name}_train.transcription",
             "words":               "etc/misc.{name}.words",
             "errors":              "etc/misc.{name}.g2p_errors",
             "dictionary":          "etc/{name}.dic",
             "phones":              "etc/{name}.phone",
             "fillers":             "etc/{name}.filler"}
# {name}.lm.DMP is missing for the language model


## setup logger
logger = logging.getLogger()
ch = logging.StreamHandler()
ch.setFormatter(logging.Formatter(fmt="{levelname:<7}: {message}", style="{"))
ch.setLevel(logging.DEBUG) # handle everything
logger.addHandler(ch)

FILLERS="""
<s> SIL
</s> SIL
<sil> SIL
""".strip()

### helpers

def all_exist(*paths):
    for p in paths:
        if not os.path.exists(p):
            logger.info("{} does not exist".format(p))
            return False
    return True

def none_exist(*paths):
    for p in paths:
        if os.path.exists(p):
            logger.info("{} exists already".format(p))
            return False
    return True

def preconditions_met(required_files, output_files):
    if not all_exist(*required_files):
        logger.info("A required file is missing, skipping.")
        return False
    if not none_exist(*output_files):
        logger.info("An output file is already there, skipping.")
        return False
    return True


### generative functions

def gen_files(snippet_dir, f_filename, t_filename, caps=False):
    """snippetdir -> fileids, transcripts

    given a directory with the structure like this:
    
    wav/
      sample_dir1/
        sample01.wav
        sample01.txt
        sample02.wav
        sample02.txt
        ...
      sample_dir2/
        ...

    the text files which have a corresponding wav file will be taken
    as transcript files and their contents will be put into the
    transcripts file.  The matching filenames will be written into the
    fileids file.

    """
    logger.info("gen_files: using snippets dir to generate fileids and transcriptions")
    if not preconditions_met([snippet_dir], [f_filename, t_filename]): return
    try:
        fileids = open(f_filename, "w")
        transcripts = open(t_filename, "w")
    except Exception as e:
        sys.stderr.write("Error openening files to write to:\n" + str(e) + "\n")
        sys.stderr.flush()
        exit(1)
    articles = os.listdir(snippet_dir)
    logger.info("Articles: {}".format(len(articles)))
    total_counter = 0
    for a in articles:
        path = os.path.join(snippet_dir, a)
        if os.path.isdir(path):
            logger.debug("Handling article: {}".format(a))
            files = os.listdir(path)
            filenames = sorted(list(set([os.path.splitext(f)[0] for f in files])))
            snippetscounter = 0
            for f in filenames:
                if f + ".txt" in files and f + ".wav" in files:
                    snippetscounter += 1
                    fileids.write(os.path.join(a, f) + "\n")
                    with open(os.path.join(path, f + ".txt")) as transcript_file:
                        transcript = transcript_file.read().strip()
                        if caps:
                            transcript = transcript_to_upper(transcript)
                        logger.debug("{article} {nr} {transcript}".format(article=a, nr=f, transcript=transcript))
                        transcript += " ({})".format(f)
                    transcripts.write(transcript + "\n")
            logger.debug("Article {} has {} snippets".format(a, snippetscounter))
            total_counter += snippetscounter
    logger.info("Total snippets: {}".format(total_counter))
    fileids.close()
    transcripts.close()


def transcript_to_upper(text):
    """converts all words to uppercase, but not the start and stop marks"""
    text = text.upper()
    text = re.sub("<S>", "<s>", text) # kinda hacky but it works I guess
    text = re.sub("</S>", "</s>", text)
    return text


def get_transcript_text(line):
    return re.match(r"^<s> (.*) </s> \(.*\)$", line.strip()).groups()[0]


def gen_wordlist(t_filename, w_filename):
    """transcripts -> words

    given a transcripts file, all the unique words will be taken and
    written into the words file.

    """
    logger.info("generating wordlist from transcriptions")
    if not preconditions_met([t_filename], [w_filename]): return
    words = set()
    with open(t_filename) as f:
        lines = f.read().splitlines()
    logger.info("Transcript lines: {}".format(len(lines)))
    for line in lines:
        text = get_transcript_text(line)
        ws = text.split()
        words = words.union(ws)
    logger.info("Unique words: {}".format(len(words)))
    words = sorted(words)
    with open(w_filename, "w") as f:
        for w in words:
            f.write(w + "\n")


def recode_sequitur_file(file_path):
    """file -> file
    """
    with open(file_path, encoding='ISO-8859-15') as f:
        content = f.read()
    with open(file_path, "w", encoding="UTF-8") as f:
        f.write(content)


def call_sequitur(g2p_path, model_path, words_path, dict_path, errors_path):
    """g2p_bin, model, words -> dictionary, errors

    given the path to an executable file which takes a model file as
    first argument and a words file as a second argument, the
    executable will be called with the given filenames and its stdout
    will be piped to the dictionary file; stderr to the errors file.
    This is must useful with the apply_model.sh file generated by the
    install_sequitur.sh script.

    """
    logger.info("generating g2p dictionary with sequitur")
    if not preconditions_met([g2p_path, model_path, words_path], [dict_path, errors_path]): return
    with open(errors_path, "w") as errors, open(dict_path, "w") as dict:
        process = subprocess.Popen([g2p_path, model_path, words_path],
                                   stdout=dict, stderr=errors)
        process.communicate()
    logger.info("files generated, sequitur files need recoding")
#    recode_sequitur_file(errors_path)
#    recode_sequitur_file(dict_path)
    


def parse_untranslatable_strings(errors_path):
    """errors -> [strings]

    parses the file with sequitur errors to find words which are not
    g2p translatable.  Returns these words in a list.

    """
    strs = []
    with open(errors_path) as errors:
        lines = errors.read().splitlines()
    for line in lines:
        if line.startswith("failed to convert"):
            string = re.match(r'^failed to convert "(.*)": translation failed$', line).groups()[0]
            strs.append(string)
    return strs


def read_untranslatable_strings(words_path, dic_path):
    """words, dictionary -> [strings]

    because the sequitur errors somehow have the wrong encoding, errors
    cannot be read directly from there. Instead, the diff of the words
    in the dic and the input words is taken.

    """
    logger.info("Reading words and dictionary to find untranslated words")
    with open(words_path) as f:
        words = f.read().splitlines()
        logger.info("{} words read".format(len(words)))
    with open(dic_path) as f:
        translations = f.read().splitlines()
        logger.info("{} translations read".format(len(translations)))
    translated_words = set([transl.split("\t")[0] for transl in translations])
    untranslated_words = [w for w in words if w not in translated_words]
    logger.info("=> {} untranslatable words found".format(len(untranslated_words)))
    return untranslated_words


def remove_lines(file_path, line_numbers_to_remove, dump_file=None):
    """file -> file

    removes the given lines identified by their line numbers from the
    given file.

    """
    removed = []
    with open(file_path) as f:
        lines = f.readlines()
    with open(file_path, "w") as f:
        for i, line in enumerate(lines):
            if i not in line_numbers_to_remove:
                f.write(line)
            else:
                if dump_file != None:
                    removed.append(line)
    if dump_file:
        with open(dump_file, "w") as f:
            for l in removed:
                f.write(line)
    
        
def remove_erroneous_entries(errors_path, fileids_path, transcript_path, dump_path):
    """errors, transcripts, fileids -> transcripts, fileids

    given the error file generated in the previous step, the words
    which couldn't be transcripted are a parsed and the transcript
    lines which contain those words are removed, together with the
    corresponding lines from the fileids file.

    """
    logger.info("using sequitur errors to remove non-translatable lines from transcriptions")
    if not preconditions_met([errors_path, fileids_path, transcript_path], [dump_path]): return
    #untransl_strs = read_untranslatable_strings(words_path, dic_path)
    untransl_strs = parse_untranslatable_strings(errors_path)
    logger.info("looking for {} words in transcripts".format(len(untransl_strs)))
    indices_to_remove = []
    with open(transcript_path) as transcripts:
        lines = transcripts.read().splitlines()
    texts = [get_transcript_text(l) for l in lines]
    for i, text in enumerate(texts):
        for s in untransl_strs:
            if s in text:
                indices_to_remove.append(i)
                break
    logger.info("found {} lines to remove".format(len(indices_to_remove)))
    input("Press enter to continue ...")
    remove_lines(fileids_path, indices_to_remove, dump_path)
    remove_lines(transcript_path, indices_to_remove)


def generate_phones(dictionary_path, phones_path):
    """dictionary -> phones

    given a dictionary file, all the used phones are parsed and
    written into the phones file, one per line.

    """
    logger.info("generating the phonelist from the dictionary")
    if not preconditions_met([dictionary_path], [phones_path]): return
    phones = set()
    phones.add("SIL") # silence
    with open(dictionary_path) as dictionary:
        lines = dictionary.readlines()
    for l in lines:
        ps = l.split()[1:]
        phones = phones.union(ps)
    phones = sorted(phones)
    logger.info("Phones: {}".format(len(phones)))
    with open(phones_path, "w") as p:
        for phone in phones:
            p.write(phone + "\n")


def split_test_train(fileids_path, transcription_path, test_share, test_fids_path, test_ts_path, train_fids_path, train_ts_path):
    logger.info("splitting fileids and transcription into testing and training")
    if not preconditions_met([fileids_path, transcription_path],
                             [test_fids_path, test_ts_path, train_fids_path, train_ts_path]): return
    with open(fileids_path) as f:
        fileids = f.read().splitlines()
    with open(transcription_path) as f:
        transcriptions = f.read().splitlines()
    pairs = list(zip(fileids, transcriptions))
    random.shuffle(pairs)
    for_testing = int(test_share * len(pairs))
    testing = pairs[:for_testing]
    training = pairs[for_testing:]
    with open(test_fids_path, "w") as f1, open(test_ts_path, "w") as f2:
        for pair in testing:
            f1.write(pair[0] + "\n")
            f2.write(pair[1] + "\n")
    with open(train_fids_path, "w") as f1, open(train_ts_path, "w") as f2:
        for pair in training:
            f1.write(pair[0] + "\n")
            f2.write(pair[1] + "\n")


def no_test(fileids_path, transcription_path, train_fids_path, train_ts_path):
    logger.info("no_test: fileids and transcripts won't be split; just copied")
    if not preconditions_met([fileids_path, transcription_path], [train_fids_path, train_ts_path]): return
    copyfile(fileids_path, train_fids_path)
    copyfile(transcription_path, train_ts_path)


def generate_fillers(fillers_path):
    """-> fillers

    this functionality takes no input, it just generates the default
    fillers file.

    """
    logger.info("generating default fillers file")
    if not preconditions_met([], [fillers_path]): return
    with open(fillers_path, "w") as f:
        f.write(FILLERS)


def modify_cfg_file(cfg_file_path):
    with open(cfg_file_path) as f:
        cfg = f.read()
    cfg = re.sub("CFG_FINAL_NUM_DENSITIES = 8", "CFG_FINAL_NUM_DENSITIES = 16", cfg)
    cfg = re.sub("CFG_N_TIED_STATES = 200", "CFG_N_TIED_STATES = 4000", cfg)
    cfg = re.sub("CFG_NPART = 1", "CFG_NPART = 24", cfg)
    cfg = re.sub('CFG_FORCE_ALIGN_MODELDIR = "$CFG_MODEL_DIR/$CFG_EXPTNAME.falign_ci_$CFG_DIRLABEL";',
                 'CFG_FORCE_ALIGN_MODELDIR = "$CFG_BASE_DIR/forcealign_model";', cfg)
    cfg = re.sub("CFG_LDA_MLLT = 'no'", "CFG_LDA_MLLT = 'yes'", cfg)
    cfg = re.sub("CFG_LDA_DIMENSION = 29", "CFG_LDA_DIMENSION = 32", cfg)
    cfg = re.sub("CFG_CONVERGENCE_RATIO = 0.1", "CFG_CONVERGENCE_RATIO = 0.04", cfg)
    cfg = re.sub('CFG_QUEUE_TYPE = "Queue"', 'CFG_QUEUE_TYPE = "Queue::POSIX"', cfg)
    with open(cfg_file_path, "w") as f:
        f.write(cfg)
    

### main function and preparing arguments

def set_log_level(verbosity):
    if verbosity == 0:
        logger.setLevel(logging.WARNING)
    elif verbosity == 1:
        logger.setLevel(logging.INFO)
    elif verbosity >= 2:
        logger.setLevel(logging.DEBUG)


def create_parent_dirs(filenames):
    dirs = []
    for fn in filenames:
        dir = os.path.dirname(fn)
        if dir != "":
            dirs.append(dir)
    for dir in dirs:
        os.makedirs(dir, exist_ok=True)


def main_everything(args):
    fns = FILENAMES.copy()
    for k in fns:
        fns[k] = os.path.join(args.prefix, fns[k])
        fns[k] = fns[k].format(name=args.name)
    create_parent_dirs(fns.values())
    gen_files(fns["snippetsdir"], fns["fileids"], fns["transcription"], caps=args.caps)
    gen_wordlist(fns["transcription"], fns["words"])
    call_sequitur(args.g2p_bin, args.g2p_model, fns["words"], fns["dictionary"], fns["errors"])
    remove_erroneous_entries(fns["errors"], fns["fileids"], fns["transcription"], fns["removed_fileids"])
    generate_phones(fns["dictionary"], fns["phones"])
    generate_fillers(fns["fillers"])
    if args.no_test:
        no_test(fns["fileids"], fns["transcription"],
                fns["train_fileids"], fns["train_transcription"])
    else:
        split_test_train(fns["fileids"], fns["transcription"], args.split,
                         fns["test_fileids"], fns["test_transcription"],
                         fns["train_fileids"], fns["train_transcription"])
    logger.info("everything finished")
        
        
def main():
    args = parser.parse_args()
    set_log_level(args.verbose)
    if args.sub_command_name == "gen":
        main_everything(args)
    elif args.sub_command_name == "cfg":
        modify_cfg_file(args.cfg_file) # TODO possibly some logging
    else:
        pass # we should never get here


if __name__ == "__main__":
    main()
