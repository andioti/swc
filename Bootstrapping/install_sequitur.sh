#! /bin/bash

# installs into ./sequitur
# creates temporary files in current working directory

# checks if all dependencies for sequitur are installed
# downloads the installation file
# sets up a virtual environment
# installs sequitur and creates a SOURCEME and a apply_model.sh file
# call it like this: ./apply_model.sh <model_file> <wordlist_file>

echo "You also need SWIG installed and a c++ compiler (e.g. gcc)"

# dependencies
command -v swig >/dev/null 2>&1 || \
    { echo >&2 "swig is required but it's not installed.  Aborting."; exit 1; }
command -v python2.7 >/dev/null 2>&1 || \
    { echo >&2 "python2.7 is required but it's not installed.  Aborting."; exit 1; }
command -v pip >/dev/null 2>&1 || \
    { echo >&2 "pip is required but it's not installed.  Aborting."; exit 1; }
#[ -f /usr/local/bin/virtualenvwrapper.sh ] || [ -f $HOME/.local/bin/virtualenvwrapper.sh ]  \
#    { echo >&2 "virtualenvwrapper is required but it's not installed.  Aborting."; exit 1; }


sequitur_filename="g2p-r1668-r3.tar.gz"  # which version to install
if [ ! -f $sequitur_filename ]; then
    wget "http://www-i6.informatik.rwth-aachen.de/web/Software/$sequitur_filename"
    tar -xf $sequitur_filename # creates directory called g2p
fi

echo "loading virtualenvwrapper"
VIRTUALENVWRAPPER_PYTHON=$(which python3)
[ -f /usr/local/bin/virtualenvwrapper.sh ] && . /usr/local/bin/virtualenvwrapper.sh # ubuntu
[ -f $HOME/.local/bin/virtualenvwrapper.sh ] && . $HOME/.local/bin/virtualenvwrapper.sh # suse

echo "Creating virtualenv 'sequitur'"
mkvirtualenv -p $(which python2.7) sequitur
echo "Installing numpy"
pip install numpy

mkdir -p sequitur
cd g2p
python setup.py install --prefix ../sequitur || \
    { echo >&2 "setup failed.  Aborting."; exit 1; }
python setup.py install --prefix ../sequitur # second run required to copy sequitur_.py
cd -

cd sequitur/lib/python2.7/site-packages/
PYTHONPATH="$(pwd)"
export PYTHONPATH
cd -

cat >"sequitur/SOURCEME" <<EOF
PYTHONPATH=$PYTHONPATH
export PYTHONPATH
EOF

cat >"sequitur/apply_model.sh" <<EOF
#! /bin/bash
# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=\$(readlink -f "\$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=\$(dirname "\$SCRIPT")
source \$SCRIPTPATH/SOURCEME

VIRTUALENVWRAPPER_PYTHON=\$(which python3)
[ -f /usr/local/bin/virtualenvwrapper.sh ] && . /usr/local/bin/virtualenvwrapper.sh # ubuntu
[ -f \$HOME/.local/bin/virtualenvwrapper.sh ] && . \$HOME/.local/bin/virtualenvwrapper.sh # suse

\$SCRIPTPATH/bin/g2p.py --model \$1 --apply \$2 --encoding=UTF-8
EOF
chmod +x sequitur/apply_model.sh
cat >"sequitur/caselessg2p.pl" <<EOF
#!/usr/bin/perl

use strict;
use warnings;
use FileHandle;
use IPC::Open2;

# wrap Sequitur G2P so that it is independent of upper/lowercasing of the input and model
# enter words line by line words can be whatever upper/lowercase you want. output is identical to Sequitur G2P with the case information preserved even if Sequitur G2P would be unable to translate given the case
# what it does: 

my $model = shift @ARGV;

my %arpa2sampa = (
#vowels:
"AO" => "O:",
"AA" => "A:",
"IY" => "i:",
"UW" => "u",
"EH" => "E",
"IH" => "I",
"UH" => "U",
"AH" => "V",
"AX" => '@',
"AE" => "{",
#diphthongs
"EY" => "eI",
"AY" => "aI",
"OW" => '@U',
"AW" => "aU",
"OY" => "OI",
#R-colored vowels
"ER" => "3`",
"AXR" => '@', # ignore that it should be @` (which maus doesn't have)
#stops
"P" => "p",
"B" => "b",
"T" => "t",
"D" => "d",
"K" => "k",
"G" => "g",
#affricates
"CH" => "TS",
"JH" => "dZ",
#fricatives
"F" => "f",
"V" => "v",
"TH" => "T",
"DH" => "D",
"S" => "s",
"Z" => "z",
"SH" => "S",
"ZH" => "Z",
"HH" => "h",
#nasals
"M" => "m",
"EM" => "m=",
"N" => "n",
"EN" => "n=",
"NG" => "N",
"ENG" => "N=",

"L" => "l",
"EL" => "l=",
"R" => "r",
"DX" => "4",
"NX" => "t", # this is clearly wrong but the best I can come up with
#semivowels
"Y" => "j",
"W" => "w",
"Q" => "?"
);

my @query;
my %translations;
while (my $line = <>) {
	chomp $line; chomp $line;
	# store keys in the lookup table
	$translations{$line} = undef;
	$translations{lc($line)} = undef;
	$translations{uc($line)} = undef;
	# store original in list to retain ordering of results
	push @query, $line;
}
my $pid = open2(*G2POUT, *G2PIN, "code-repository/sequitur/apply_model.sh $model -");
# print STDERR "querying for:\n" . (join "\n", keys %translations) . "\n";
print G2PIN join "\n", keys %translations;
close G2PIN;
waitpid($pid, 0);
while (my $line = <G2POUT>) {
	chomp $line; chomp $line;
	(my ($word, @phonemes)) = split " ", $line;
	$translations{$word} = join " ", map { $arpa2sampa{$_} } @phonemes;
}
close G2POUT;
foreach my $word (@query) {
	my $translation = "";
	if (defined $translations{$word}) {
		$translation = $translations{$word};
	} elsif (defined $translations{lc($word)}) {
		$translation = $translations{lc($word)};
	} elsif (defined $translations{uc($word)}) {
		$translation = $translations{uc($word)};	
	}
	print "$word\t$translation\n";
}
EOF
chmod +x sequitur/caselessg2p.pl

echo
if [ -x ./sequitur/bin/g2p.py ]; then
    rm -r g2p $sequitur_filename
    echo "Install successfull. To set your PYTHONPATH:"
    echo "source sequitur/SOURCEME"
    echo 
    echo "Try './sequitur/bin/g2p.py --help'"
else
    echo "Installation failed"
    exit 1
fi
