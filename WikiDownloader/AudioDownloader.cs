﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using Util;

namespace WikiDownloader
{
    abstract class AudioDownloader : Downloader
    {
        public void Run(Configuration config)
        {
            string apiURL = config.apiURL;
            string category = config.category;
			string articlesDirectory = config.outputDirectory;
			string readerDirectory = articlesDirectory + "/../readers/";
            string indexURL = config.indexURL;

            //Get list of all spoken pages

            //IList<long> pages = new long[] { 2552494 };//Used this for testing ;)
            IList<long> pages = GetPagesInCategory(apiURL, category).Select(v => v.Key).ToArray();
            Log("Info", string.Format("{0} contains {1} articles.", category, pages.Count));

            //Get metainformation for all these pages
            var articleMetadataList = GetPageMetadata(apiURL, pages);
            Log("Info", string.Format("Received metainformation for {0} pages.", articleMetadataList.Count));

            Directory.CreateDirectory(articlesDirectory);//Does nothing if it already exists
			foreach (var articleMetadata in articleMetadataList)
            {
				if (config.additionalArgs != null && !config.additionalArgs.Contains (EncodeFileName(articleMetadata.title))) {
                    Log ("Info", "Skipping " + articleMetadata.title + " as download is restricted by runtime arguments.");
                    continue;
                }
                articleMetadata.identifier = EncodeFileName (articleMetadata.title);
                articleMetadata.language = getLanguage ();
                Log("Info", "Page: " + articleMetadata.title);
                var pageInfo = new SpokenArticleInfo();
                pageInfo.article = articleMetadata;
                SpokenArticleInfo oldPageInfo = new SpokenArticleInfo();

                //Create a directory for the page, if it does not already exist
				string articleDir = Path.Combine(articlesDirectory, EncodeFileName(pageInfo.article.title));
                Directory.CreateDirectory(articleDir);
                try
                {
                    //Read information from previous run:
                    try
                    {
                        string json = File.ReadAllText(Path.Combine(articleDir, "info.json"));
                        oldPageInfo = Json.Decode<SpokenArticleInfo>(json);
						Log("Info", "found existing info.json");
                    }
                    catch (FileNotFoundException)
                    {
                    }

                    //Retrieve audio file name
                    if (oldPageInfo.article.revision != pageInfo.article.revision)
                    {
                        pageInfo.article_parsed = ParseArticle(apiURL, articleDir, pageInfo.article.pageid);
                    }
                    else
                    {
                        pageInfo.article_parsed = oldPageInfo.article_parsed;
                    }

                    if (pageInfo.article_parsed.audio_file_titles == null || pageInfo.article_parsed.audio_file_titles.Length == 0)
                    {
                        pageInfo.error = "Can't find audio file name";
                        continue; //Continue with next page
                    }

                    //Retrieve audio metadata
                    pageInfo.audio_files = pageInfo.article_parsed.audio_file_titles
						.Select(file_title => GetAudioMetadata(apiURL, file_title)).ToArray();

                    if (string.IsNullOrEmpty(pageInfo.audio_files[0].downloadurl))
                    {
                        pageInfo.error = "Can't find audio file metadata";
                        continue; //Continue with next page
                    }

                    //Download audio file
                    for (int i = 0; i < pageInfo.audio_files.Length; i++)
                    {
                        var file = pageInfo.audio_files[i];
                        string name = pageInfo.audio_files.Length > 1 ? ("audio" + (i + 1) + ".ogg") : "audio.ogg";
						string outputPath = Path.Combine(articleDir, name);
						if (!File.Exists(outputPath) && file.downloadurl != null && !config.skipAudio)
                        {
							Log("Info", "Downloading: " + file.downloadurl + " and storing as: " + outputPath);
							file.metadataURL = indexURL + "title=" + EncodeFileName(file.title);
                            DownloadFile(file.downloadurl, outputPath);
                        }
                    }

                    //Retrieve audio info page
                    if (oldPageInfo.audio_files == null || oldPageInfo.audio_files.Length == 0 || pageInfo.audio_files[0].timestamp != oldPageInfo.audio_files[0].timestamp)
                    {
                        pageInfo.audio_file_parsed = ParseAudioPage(apiURL, pageInfo.audio_files[0], pageInfo.article.pageid, articleDir);
                    }
                    else
                    {
                        //Nothing changed ...
                        pageInfo.audio_file_parsed = oldPageInfo.audio_file_parsed;
                        continue; //Continue with next page
                    }

                    if (pageInfo.GetOldId() <= 0)
                    {
                        pageInfo.error = "Can't find oldid";
                        continue; //Continue with next page
                    }

                    //Download text of old article version
                    if (pageInfo.GetOldId() != oldPageInfo.GetOldId())
                    {
                        Log("Debug", "Downloading article revision " + pageInfo.GetOldId() + ".");
                        DownloadRevision(apiURL, articleDir, pageInfo.GetOldId());
                        pageInfo.article_parsed.oldURL = indexURL + "title=" + pageInfo.article.title + "&oldid=" + pageInfo.GetOldId();
                    }

					//make a symbolic link from readerDB/$reader to article
					CrossLinkWithReader(readerDirectory, pageInfo.GetReader(), articlesDirectory, pageInfo.article.identifier);
                }
                finally
                {
                    if (!string.IsNullOrEmpty(pageInfo.error))
                        Log("ERROR", pageInfo.error + " for page: " + pageInfo.article.title);
                    File.WriteAllText(Path.Combine(articleDir, "info.json"), Json.Encode(pageInfo));
                }
            }
        }

		private static void CrossLinkWithReader(string readersDir, string reader, string articlesDir, string article) {
			Directory.CreateDirectory (readersDir); // does not harm if exists
			string readerDir = Path.Combine (readersDir, EncodeFileName(reader));
			Directory.CreateDirectory (readerDir);
			File.WriteAllText (Path.Combine (readerDir, article + ".link"), 
				"#!/bin/sh\n" +
				"ln -s " + Path.GetFullPath (articlesDir) + "/" + article + " " + Path.GetFullPath(readerDir) + "/" + article + "\n" +
				"ln -s " + Path.GetFullPath (readerDir) + " " + Path.GetFullPath (articlesDir) + "/" + article + "/reader");
		}

        // download article text of a given revision (oldID) in all versions (txt, xml, html)
        private static void DownloadRevision(string api, string dir, long oldId)
        {
            string url = api
                + "action=parse"
                + "&format=json"
                + "&oldid=" + oldId
                + "&redirects="
                + "&prop=text%7Cwikitext"
                + "&generatexml=";

            string json = DownloadTextFile(url);
            dynamic obj = Json.Decode(json);
            if (HasProperty(obj, "error"))
            {
                Log("ERROR", "Can't download revision " + oldId);
                return;
            }

            dynamic parse = obj.parse;
            string wikitext = GetProperty(parse.wikitext, "*");
            string parsetree = GetProperty(parse.parsetree, "*");
            string html = GetProperty(parse.text, "*");

            File.WriteAllText(Path.Combine(dir, "wiki.txt"), wikitext);
            File.WriteAllText(Path.Combine(dir, "wiki.xml"), parsetree);
            File.WriteAllText(Path.Combine(dir, "wiki.html"), html);
        }

        private static AudioMetadata GetAudioMetadata(string api, string title)
        {
            string url = api
                + "action=query"
                + "&prop=imageinfo"
                + "&format=json"
                + "&iiprop=timestamp%7Ccanonicaltitle%7Curl"
                + "&titles=" + Uri.EscapeDataString(title);

            string json = DownloadTextFile(url);
            dynamic obj = Json.Decode(json);
            var result = new AudioMetadata();

            if (!HasProperty(obj, "query"))
                return result;

            var pages = (IDictionary<string, object>)obj.query.pages;
            dynamic page = pages.First().Value;


            if (HasProperty(page, "pageid"))
                result.pageid = (long)page.pageid;
            result.title = page.title;
            if (HasProperty(page, "imagerepository"))
                result.repository = page.imagerepository;
            if (HasProperty(page, "imageinfo"))
            {
                IEnumerable revisions = page.imageinfo;
                dynamic revision = revisions.Cast<object>().First();//TODO: Find latest instead of first

                result.downloadurl = revision.url;
                result.timestamp = revision.timestamp.ToString();
            }
            return result;
        }

		private ExtractedData ParseAudioPage(string api, AudioMetadata audio_file, long article_pageid, string articleDir)
        {
			Log("Info", "Parsing the Audio Page...");
            string actual_apiURL;
            string actual_title;
            if (audio_file.repository == "shared")//shared means commons
            {
                actual_apiURL = "http://commons.wikimedia.org/w/api.php?";
                actual_title = "File:" + audio_file.title.Split(new char[] { ':' }, 2)[1];
            }
            else
            {
                actual_apiURL = api;
                actual_title = audio_file.title;
            }

            string query = ""
                + "action=parse"
                + "&format=json"
                + "&page=" + Uri.EscapeDataString(actual_title)
                + "&prop=wikitext"
                + "&generatexml=";

            string json = DownloadTextFile(actual_apiURL + query);
            dynamic obj = Json.Decode(json);
            string wikitext = GetProperty(obj.parse.wikitext, "*");
            string parsetree = GetProperty(obj.parse.parsetree, "*");

			File.WriteAllText(Path.Combine(articleDir, "audiometa.txt"), wikitext);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(parsetree);

            var result = ParseAudioPageXML(audio_file, xmlDoc);

            if (result.article_oldid <= 0 || result.reader == null)
            {
                //Parse the generic template to fill in missing information
                foreach (var t in GetTemplatesWithTitleContaining(xmlDoc, "information"))
                {
                    foreach (var part in t.GetChildElements("part"))
                    {
                        if (!part.HasChildElement("name"))
                            continue;

                        string key = part.GetChildElement("name").InnerText.Trim().ToLowerInvariant();
                        string value = part.GetChildElement("value").GetInnerTextOrNull();
                        if (key == "author")
                        {
                            if (result.reader == null)
                                result.reader = value;
                        }
                        else if (key == "date")
                        {
                            if (!result.is_date_normalized)
							{
								Log("Info", "found date from date element in audio page:" + value);
								NormalizeDateIfPossible(result, value);
							}

                        }
						else if (key == "oldid")
						{
							Match m;
							if (value.TryMatch(@"\d{5,}", out m)) {
								Log("Info", "found oldid in information Template");
								result.article_oldid = long.Parse(m.Value);
								result.oldid_quality = 2;
							}
						}

                    }

                }

                XmlElement template;

                if ((template = GetTemplateByTitle(xmlDoc, "original upload date")) != null)
                {
                    String maybeDate = template.GetChildElement("part").GetChildElement("value").GetInnerTextOrNull();
					if (maybeDate != null)
						Log("Info", "found original upload date element in audio page: " + maybeDate);
						NormalizeDateIfPossible(result, maybeDate);
                }
            }
            GuessOldidIfNeccessary(result, api, article_pageid);
			Log("Info", "Extracted from Audio Page XML: "
				+ "oldid=" + result.article_oldid
				+ ", date_read="+ result.date_read
				+ ", reader=" + result.reader
				);
            return result;
        }

        protected virtual DateTime? ParseDate(string str)
        {
            return DateParser.ParseDate(str);
        }

		protected virtual ExtractedData ParseAudioPageXML(AudioMetadata audio_file, XmlDocument xmlDoc)
		{
			Log("Info", "Parsing audio page xml...");
			var result = new ExtractedData { article_oldid = -1 };
			XmlElement template;
			if ((template = GetTemplatesWithTitleContaining(xmlDoc, "spoken").FirstOrDefault()) != null)
			{
				Log ("Info", template.OuterXml);
				string time_string = null;
				string date_string = null;
				//Parse the spoken wikipedia template
				foreach (var part in template.GetChildElements("part"))
				{
					if (!part.HasChildElement("name"))
						continue;

					string key = part.GetChildElement("name").InnerText.Trim().ToLowerInvariant();
					string value = part.GetChildElement("value").GetInnerTextOrNull();
					if (key == "user_name")
					{
						result.reader = value;
					}
					else if (key == "date")
					{
						date_string = value;
						NormalizeDateIfPossible(result, value);
					}
					else if (key == "time")
					{
						time_string = value;
					}
					else if (key == "oldid")
					{
						long oldid;
						if (long.TryParse(value, out oldid)) {
							Log("Info", "found oldid in spoken wikipedia Template");
							result.article_oldid = oldid;
							result.oldid_quality = 2;
						}
					}
				}


				var match = Regex.Match(template.InnerText, @"http://[^\s]+&oldid=([0-9]+)");
				if (match.Success)
				{
					Log("Info", "found oldid in spoken wikipedia Template via url");
					string oldid_string = match.Groups[1].Value;
					result.article_oldid = long.Parse(oldid_string);
					result.oldid_quality = 2;
				}
				if (!string.IsNullOrWhiteSpace(time_string))
					NormalizeDateIfPossible(result, date_string + " " + time_string);
			}
			else
			{
				Log("Warning", "File description lacks the template: " + audio_file.title);
			}
			return result;
		}

		/// <summary>
		///   Set date_read in data to datestr if it can be parsed.
		///   Otherwise, only set if it the previous date couldn't be parsed as well
		/// </summary>
		private void NormalizeDateIfPossible(ExtractedData data, String datestr)
		{
			DateTime? date = ParseDate(datestr);
            if (date != null)
            {
				data.is_date_normalized = true;
                data.date_read = ((DateTime)date).ToString("yyyy-MM-dd HH:mm:ss");
				Log("Info", "setting normalized date to " + data.date_read);
            }
			else if (!data.is_date_normalized)
			{
				Log("Info", "couldn't parse but still setting date to " + datestr);
				data.date_read = datestr;
			}
		}


        private void GuessOldidIfNeccessary(ExtractedData result, string api, long article_pageid)
        {
	        // only need to guess if we don't have an oldid yet
	        if (result.article_oldid > 0 || !result.is_date_normalized)
		        return;
	        Log("Info", "guessing OldId from Article Metadata...");
			DateTime date = (DateTime)(ParseDate(result.date_read));
		    if (result.date_read != null)
		    {
			    result.article_oldid = GetRevisionByDate(api, article_pageid, date);
			    result.oldid_quality = 1;
			    Log("Info", "successfully guessed oldid from date: " + result.article_oldid);
		    }
		    else if (result.date_read != null && result.date_read != "Date")
		    {
			    Log("Info", "Can't find oldid for date \"" + result.date_read + "\"");
		    }
	    }

        private static long GetRevisionByDate(string api, long pageid, DateTime date)
        {
            string url = api
                + "action=query"
                + "&prop=revisions"
                + "&format=json"
                + "&rvprop=ids%7Ctimestamp"
                + "&rvlimit=1"
                + "&rvstart=" + Uri.EscapeDataString(date.ToString("yyyy-MM-dd HH:mm:ss"))
                + "&rvdir=older"
                + "&pageids=" + pageid
                + "&redirects=";

            string json = DownloadTextFile(url);
            dynamic obj = Json.Decode(json);

            dynamic page = GetProperty(obj.query.pages, pageid.ToString());
            if (!HasProperty(page, "revisions"))
                return -1;
            IList revisions = page.revisions;
            if (revisions.Count == 0)
                return -1;
            dynamic revision = revisions[0];
            return (long)revision.revid;
        }

        private ExtractedData ParseArticle(string api, string dir, long pageid)
        {
            Log("Info", "Downloading pageID " + pageid + " ... ");

            string url = api
                + "action=parse"
                + "&format=json"
                + "&pageid=" + pageid
                + "&prop=wikitext"
                + "&generatexml=";

            string json = DownloadTextFile(url);
            dynamic obj = Json.Decode(json);


            string wikitext = GetProperty(obj.parse.wikitext, "*");
            File.WriteAllText (Path.Combine (dir, "current.txt"), wikitext);
            string parsetree = GetProperty(obj.parse.parsetree, "*");
            File.WriteAllText (Path.Combine (dir, "current.xml"), parsetree);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(parsetree);
            var result = ParseArticleXML(pageid, xmlDoc);
			NormalizeDateIfPossible(result, result.date_read);
            GuessOldidIfNeccessary(result, api, pageid);
	        Log("Info", "Extracted from article Page: "
	                    + "oldid=" + result.article_oldid
	                    + ", date_read="+ result.date_read
	                    + ", reader=" + result.reader
	        );

            return result;
        }

        protected abstract ExtractedData ParseArticleXML (long pageid, XmlDocument xmlDoc);

		protected abstract string getLanguage();

        public static XmlElement GetTemplateByTitle(XmlDocument xmlDoc, string title)
        {
            return GetTemplates(xmlDoc).FirstOrDefault(el =>
                string.Equals((el.GetChildElement("title").InnerText).Trim(), title, StringComparison.InvariantCultureIgnoreCase));
        }

        public static IEnumerable<XmlElement> GetTemplatesWithTitleContaining(XmlDocument xmlDoc, string title)
        {
            return GetTemplates(xmlDoc).Where(el =>
                (el.GetChildElement("title").InnerText).IndexOf(title, StringComparison.InvariantCultureIgnoreCase) >= 0);
        }

        public static IEnumerable<XmlElement> GetTemplates(XmlDocument xmlDoc)
        {
            return xmlDoc.EnumerateTree()
                .OfType<XmlElement>()
                .Where(el =>
                    el.Name == "template" &&
                    el.HasChildElement("title"));
        }

        private static List<PageMetadata> GetPageMetadata(string api, IEnumerable<long> pageIds)
        {
			var result = new List<PageMetadata>();
            using (var enumerator = pageIds.GetEnumerator())
            {
                while (true)
                {
                    //request 100 pages at once, until all are processed.
                    List<long> currentPages = new List<long>();
                    while (currentPages.Count < 50 && enumerator.MoveNext())
                        currentPages.Add(enumerator.Current);

                    if (currentPages.Count == 0)
                        break;

                    string pagelist = string.Join("|", currentPages.Select(id => id.ToString()).ToArray());

                    string url = api
                        + "action=query"
                        + "&prop=info"
                        + "&format=json"
                        + "&inprop=url%7Cdisplaytitle"
                        + "&pageids=" + Uri.EscapeDataString(pagelist);

                    string json = DownloadTextFile(url);
			        dynamic data = Json.Decode(json);
					var pageInfos = (IDictionary<string, object>)data.query.pages;
                    foreach (long pageid in currentPages)
                    {
                        dynamic info = pageInfos[pageid.ToString()];
                        result.Add(new PageMetadata
                        {
                            pageid = pageid,
                            revision = (long)info.lastrevid,
                            title = info.title,
                            canonicalurl = info.canonicalurl
                        });
                    }
                }
            }
            return result;
        }
    }
}
