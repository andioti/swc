﻿using System;
using System.IO;
using System.Net;

namespace WikiDownloader
{
    public class StreamSlice : Stream
    {
        private Stream _inner;
        private long _length;
        private long _offset;
        private long _remainingLength;
        private bool _ownsInner;

        public StreamSlice(Stream inner, long length, bool ownsInner)
        {
            _inner = inner;
            if(inner.CanSeek)
                _offset = inner.Position;
            _length = length;
            _remainingLength = length;
            _ownsInner = ownsInner;
        }

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanSeek
        {
            get { return _inner.CanSeek; }
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override bool CanTimeout
        {
            get { return _inner.CanTimeout;}
        }

        public override void Flush()
        {
            throw new NotSupportedException();
        }

        public override long Length
        {
            get { return _length; }
        }

        public override long Position
        {
            get
            {
                return _inner.Position - _offset;
            }
            set
            {
                _inner.Position = value + _offset;
                _remainingLength = _length - Position;
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            if (count > _remainingLength)
                count = (int)_remainingLength;
            int n = _inner.Read(buffer, offset, count);
            if (n > 0)
                _remainingLength -= n;
            else if (_remainingLength > 0)
                throw new WebException("Unexpected end of Stream");
            return n;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotSupportedException();
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotSupportedException();
        }

        public override int ReadByte()
        {
            if (_remainingLength <= 0)
                return -1;
            int result = _inner.ReadByte();
            if (result >= 0)
                _remainingLength--;
            else if (_remainingLength > 0)
                throw new WebException("Unexpected end of Stream");
            return result;
        }

        public override int ReadTimeout
        {
            get
            {
                return _inner.ReadTimeout;
            }
            set
            {
                _inner.ReadTimeout = value;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _ownsInner)
                _inner.Close();
        }

    }
}
