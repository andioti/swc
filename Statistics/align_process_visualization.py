#! /usr/bin/env python3

# Copyright (c) 2017, Arne Köhn <koehn@informatik.uni-hamburg.de>
# All rights reserved.
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.

# This files reads log files from the aligner and shows what parts where aligned
# at each step.
# arg1: how wide the visualization is
# arg2: time in ms(?) of the corresponding audio file
# arg3: log file to analyze

import sys
import re
import datetime

datere = re.compile("(.*) swc.aligner.api.DefaultSpeechAligner align")
rangere = re.compile(r"INFO: Aligning level \d frame (\d+):(\d+) to text \[(.*)\] range .*")
acceptre = re.compile("^INFO: Word accepted in level .*")
# datetime.datetime.strptime(datestr, "%b %d, %Y %I:%M:%S %p")

def create_data(flo, maxdur):
    times = []
    ranges = []
    words = []
    accepted = []
    num_accepted = 0
    curr_timestamp = None
    for line in flo:
        if acceptre.match(line):
            num_accepted += 1
        datematch = datere.match(line)
        if datematch:
            curr_timestamp = datetime.datetime.strptime(datematch.groups()[0], "%b %d, %Y %I:%M:%S %p")
            continue
        rangematch = rangere.match(line)
        if rangematch:
            s_frame = int(rangematch.groups()[0])
            e_frame = int(rangematch.groups()[1])
            if e_frame > maxdur:
                e_frame = maxdur
            numwords = len(rangematch.groups()[2].split(","))
            ranges.append((s_frame, e_frame))
            words.append(numwords)
            times.append(curr_timestamp)
            accepted.append(num_accepted)
            num_accepted = 0
    # append last timestamp when everything is finished
    times.append(curr_timestamp)
    return (times, ranges, words, accepted)

def visualize_data(data, width=100.0):
    width = width*1.0 # ensure float-ness
    (times, ranges, words, accepted) = data
    accepted = accepted[1:]
    durations = []
    for i in range(1,len(times)):
        durations.append((times[i] - times[i-1]))
    multiplyer = width/ranges[0][1]
    for i in range(len(ranges)):
        # print(ranges[i])
        space_before = round(ranges[i][0]*multiplyer)
        space_aligned = round((ranges[i][1]-ranges[i][0])*multiplyer)
        space_after = round(width) - space_before - space_aligned
        print(" " * space_before, end="")
        print("-" * space_aligned, end="")
        print(" " * space_after, end="")
        print(" audio: " + str(round((ranges[i][1]-ranges[i][0])*100.0/ranges[0][1],4)), end="%")
        print(" words: " + str(round(words[i]*100.0/words[0],4)), end="%")
        print(" min:" + str(durations[i]), end="")
        print(" words: " + str(words[i]), end="")
        print(" acc.: " + str(accepted[i]))
    
        
if __name__ == "__main__":
    data = create_data(open(sys.argv[3]), int(sys.argv[2]))
    visualize_data(data, int(sys.argv[1]))
    
