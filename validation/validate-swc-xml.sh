#! /bin/bash
# authors: Arne Köhn, Timo Baumann
# license: GPLv3/Apache 2.0/whatever

# AWK magic:
# 1. set record separator (RS) to either opening or closing angular bracket
# 2. set output record separator (ORS) to the empty string
# 3. print a line only if the line number is even (NR%2)
awkstrip='awk -vRS="<|>" -vORS="" NR%2'

if [[ $# == 0 ]]; then
    echo >&2 Usage: $0 path/to/articles/dir [cdata-equiv-output-dir]
    echo >&2 "(the directory containing all articles)"
    echo >&2 will search for all files ending in swc
    echo >&2 and validate them against the swc schema
    echo >&2 and compare it to the original wiki.html
    echo >&2 to see whether the CDATA is identical
	echo >&2 Optional second argument stores the stripped cdata.
	echo >&2 If not given, a tmpdir is created for that.
    exit
fi
trang $(dirname $0)/swc.rnc $(dirname $0)/swc.rng

if [[ $# == 1 ]]; then
    outdir=$(mktemp --tmpdir -d "validation.XXX")
else
    outdir=$2
fi
echo Writing cdata equivalence output to $outdir

for file in `find $1 -name '*.swc'`; do
	## Unfortunately, non-breaking space is not treated as standard whitespace
	## during schema validation. otoh, it is impossible to impose restrictions on text
	## in elements that have sub-elements.  See
	## http://www.relaxng.org/compact-tutorial-20030326.html#id2814737
	## We therefore replace all unusual space characters with normal spaces
	## for schema validation.
	## Python might seem a bit heavy for that but
	## - sed does not support unicode classes
	## - perl claims "world-class support" for unicode but also can't match all whitespace
	##   in the unicode general category Zs (Separator, Space), e.g. code point #x3000
	##  -- Arne
    xmllint --noout --relaxng \
			$(dirname $0)/swc.rng \
			<(python3 -c \
					  'import re,sys; r=re.compile("\s",re.UNICODE);[print(r.sub(" ",x)) for x in sys.stdin]' \
					  < $file \
			 ) > /dev/null
    if [ $? -ne 0 ]; then
        echo "ERROR: not valid according to RelaxNG schema: $file"
    else
        echo "INFO: valid according to RelaxNG schema: $file"
    fi
    article=$(basename "$(dirname "$file")")
    fname=$(basename "$file" .swc)
    indir=$(dirname "$file")
    filetext="$outdir/$article-$fname.text"
    origtext="$outdir/$article-wiki.origtext"
    # the sed command below replaces the HTML entity with a non-breaking space
    # NOTE: what may look like a space is a nbsp   ---->↓<--here
    eval "$awkstrip '$indir/wiki.html'" | sed 's/&#160;/ /g' \
		| perl -Mopen=locale -pe 's/&#x([\da-f]+);/chr hex $1/gie' > "$origtext"
	# the perl code converts other html entities

	# the generated file has one additional newline at the start and one at the end
	# because of the <xml> declaration and the wrapping of the cdata-equivalent part into a root element.
	# the sed script strips the first empty line.
    eval "$awkstrip '$file'"  | sed "1{N;s/\n//;}" > "$filetext"
	# strip the last charachter (additional newline)
	truncate -s -1 "$filetext"
    diff "$origtext" "$filetext" > /dev/null
    if [ $? -ne 0 ]; then
        echo "ERROR: text differences in $file"
    else
        echo "INFO: text correspondence in $file"
    fi
done

# creates a list of error occurences that are not just additional newlines,
# Format: article tab number of differing lines
cd $outdir
for i in *.origtext; do
    tokenized=${i%-wiki.origtext}-tokenized.text
    echo -n $i $'\t'; diff -u $i $tokenized | grep "^-" | wc -l;
done | grep -v $'\t0' | sort -n -k 2 > num_non_newline_errors
