using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;

public static class Program
{
	public static int Main(string[] args)
	{
		TextReader input = Console.In;
		if (args.Length > 0) {
			input = File.OpenText (args[0]);
		}
		string line;
		while ((line = input.ReadLine()) != null) {
			Match m = Regex.Match (line, @"^(.*)[(\d+)\s*:\s*(\d+)]$");
			if (m != null && m.Success)
				Console.WriteLine ("{0}\t{1}\t{2}", m.Groups [2].Value, m.Groups [3].Value, m.Groups [1].Value);
			else
				Console.WriteLine (line);
		}

		return 0;
	}
}