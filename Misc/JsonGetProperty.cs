using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Util;

namespace Statistics
{
    class Program
    {
        static void Main(string[] args)
        {
			string text = File.ReadAllText (args [0]);
			object obj = Json.Decode (text);
			string[] path = args [1].Split (new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
			foreach (var part in path) {
				obj = ((IDictionary<string,object>)obj) [part];
			}
			Console.Write (obj);
        }
		
		private static dynamic HasProperty(dynamic @object, string key)
		{
			return ((IDictionary<string, object>)@object).ContainsKey(key);
		}
    }
}
