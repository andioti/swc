package swc.statistics;

import org.jdom2.Document;
import org.jdom2.Element;
import swc.data.SwcXmlUtil;

import java.util.List;

/**
 * Created by felix on 25/12/16.
 */
public class DocStats {

    private SwcXmlUtil sxu;

    public DocStats(Document document) {
        sxu = new SwcXmlUtil(document);
    }


    public double getPercentageAlignedTokens() {
        return getPercentageAligned(sxu.getNormalizedTokens());
    }

    public double getPercentageAlignedSentences() {
        return getPercentageAligned(sxu.getSentenceLikeElements());
    }

    private double getPercentageAligned(List<Element> elements) {
        double totalCount = elements.size();
        double alignedCount = elements.stream().filter(sxu::hasCompleteTiming).count();
        return alignedCount / totalCount;
    }
}
