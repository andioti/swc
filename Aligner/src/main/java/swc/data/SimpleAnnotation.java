package swc.data;

import edu.cmu.sphinx.util.TimeFrame;

/**
 * Created by felix on 22/11/16.
 */
public class SimpleAnnotation implements Annotation {

    private final TimeFrame timeFrame;
    private final String label;

    public SimpleAnnotation(TimeFrame timeFrame, String label) {
        assert timeFrame != null;
        assert label != null;

        this.timeFrame = timeFrame;
        this.label = label;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public TimeFrame getTimeFrame() {
        return timeFrame;
    }
}
