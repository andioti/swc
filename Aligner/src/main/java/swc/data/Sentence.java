package swc.data;

import edu.cmu.sphinx.util.TimeFrame;

import java.util.ArrayList;

@SuppressWarnings("serial")
public class Sentence extends ArrayList<Word> {

    public boolean isAligned() {
        for (Word w : this) {
            if (! w.isAligned())
                return false;
        }
        return true;
    }

    /**
     * returns the entire timeframe of the sentence.
     * Warning: This only makes sense if the whole sentence is aligned.
     * @return
     */
    public TimeFrame getTimeFrame() {
        assert isAligned();
        Long s = Long.MAX_VALUE, e = Long.MIN_VALUE;
        for (Word w : this)
            for (Token t : w.parts) {
                if (t.isAligned()) {
                    if (t.getTimeFrame().getStart() < s)
                        s = t.getTimeFrame().getStart();
                    if (t.getTimeFrame().getEnd() > e)
                        e = t.getTimeFrame().getEnd();
                }
            }
        return new TimeFrame(s, e);
    }
}
