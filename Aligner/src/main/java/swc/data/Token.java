package swc.data;

import edu.cmu.sphinx.util.TimeFrame;

public class Token implements Annotation {
	private String spelling;
	private TimeFrame timeframe;
	
	public Token(String spelling) {
		this.spelling = spelling;
	}
	
	public String getSpelling() {
		return spelling;
	}

	@Override
	public String getLabel() {
		return getSpelling();
	}
	
	public void setTimeFrame(TimeFrame tf) {
		timeframe = tf;
	}


	public TimeFrame getTimeFrame() {
		return timeframe;
	}

	public boolean isAligned() {
		return getTimeFrame() != null;
	}

}
