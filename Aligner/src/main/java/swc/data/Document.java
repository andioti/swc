package swc.data;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class Document extends ArrayList<Paragraph> {
	
	public static final Logger logger = LoggerFactory.getLogger(Document.class);

	public List<Token> getNormalizedTokens() {
		List<Token> tokens = new ArrayList<>();
		logger.info(String.format("Paragraphs: %d", this.size()));
		for (Paragraph p : this) {
			logger.info(String.format("Sentences: %d", p.size()));
			for (Sentence s : p) {
				logger.info(String.format("Words: %d", s.size()));
				for (Word w : s) {
					tokens.addAll(w.parts);
				}
			}
		}
		return tokens;
	}
	
	public List<Word> getWords() {
		List<Word> words = new ArrayList<>();
		for (Paragraph p : this) {
			for (Sentence s : p) {
				words.addAll(s);
			}
		}
		return words;
	}

	public List<Sentence> getSentences() {
		List<Sentence> sentences = new ArrayList<>();
		for (Paragraph p : this) {
			sentences.addAll(p);
		}
		return sentences;
	}
}
