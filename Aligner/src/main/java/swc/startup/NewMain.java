package swc.startup;

import marytts.config.LanguageConfig;
import marytts.config.MaryConfig;
import marytts.exceptions.MaryConfigurationException;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.action.StoreTrueArgumentAction;
import net.sourceforge.argparse4j.inf.*;
import org.apache.commons.io.IOUtils;
import org.jdom2.Content;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.located.LocatedElement;
import org.jdom2.located.LocatedJDOMFactory;
import org.jdom2.located.LocatedText;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import swc.aligner.api.DefaultSpeechAligner;
import swc.aligner.api.SpeechAligner;
import swc.data.SwcXmlUtil;
import swc.snippetextractor.KaldiSnippetExtractor;
import swc.transcriptextractor.DocumentInitializer;
import swc.transcriptextractor.MaryTokenization;
import swc.io.DataIO;
import swc.maus.Maus;
import swc.snippetextractor.InvalidInputException;
import swc.snippetextractor.SnippetExtractor;
import swc.transcriptextractor.TranscriptExtractor;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;

import static java.lang.System.exit;

public class NewMain {

	private static final Logger logger = LoggerFactory.getLogger(NewMain.class);

	private static final Format rawXmlFormat = Format.getRawFormat().setLineSeparator("\n");

	public static void main(String[] args) throws Exception {
		ArgumentParser parser = ArgumentParsers.newArgumentParser("Aligner").defaultHelp(true)
				.description("The SWC Aligner");

		Subparsers subparsers = parser.addSubparsers().dest("subparser_name");

//		Subparser e2 = subparsers.addParser("extrtrans")
//				.description("This step takes wiki HTML and creates the initial SWC XML file. " +
//						"The structural info from the HTML file is kept, such as sections and paragraphs. " +
//						"Also in this step, certain portions of the document are wrapped in an 'ignore' tag " +
//						"to signalize that these parts are not read out in the spoken version of the article. " +
//						"This includes parts to the document which are not really content, such as [Bearbeiten] in " +
//						"the section headers, as well as whole sections which are simply not read out, such as 'Weblinks'. " +
//						"This filtering requires a language to be specified.\n" +
//						"The introductory text which is read for every article is also added in this step, which " +
//						"also requires a language to be specified.");
//		e2.addArgument("article_dir").help("The location of the wiki.html and info.json.");
//		e2.addArgument("output_file").help("The filename for the output file.");
//		e2.addArgument("-l", "--lang").help("The language used for filtering sections and adding the preamble." +
//				"If no language is given, no preamble will be added and sections won't be ignored.");

//		Subparser no = subparsers.addParser("toknorm")
//				.description("This step takes basic SWC XML containing no tokenization and adds tokenization and normalization. " +
//						"Normalization can be disabled.  Normalization includes splitting and expanding abbreviations and expanding " +
//						" digits into words.\n" +
//						"Even for disabled normalization a language needs to be given, because tokenization also relies on the language.\n" +
//						"This step also adds the pronounciation nodes to the tokens, which receive timing in the 'align' step.");
//		no.addArgument("lang").choices("de", "en").help("The language used for tokenization and normalization.");
//		no.addArgument("input").help("The path to the SWC XML file containing sections and paragraphs which should be tokenized and normalized.");
//		no.addArgument("output").help("The path where the generated output file should be written to.");
//		no.addArgument("-n", "--null_normalize").help("Disables normalization.").action(new StoreTrueArgumentAction()).setDefault(false);

		Subparser al = subparsers.addParser("align")
				.description("Takes a transcript containing tokens which are read out, an audio file corresponding to the " +
						"transcript and adds timings to the tokens.  This is the alignment process.  The new file, containing " +
						"the alignments is written to the given path.\n" +
						"For the alignment various model files are needed.");
		al.addArgument("acoustic_model").help("The acoustic model (a path to a directory)");
		al.addArgument("g2p").help("The g2p model as an .ser file");
		al.addArgument("dict").help("The g2p dictionary.  Can be empty or simply generated from the g2p model " +
				"to save time"); // TODO could be optional, simply an empty dict
		al.addArgument("audio").help("The audio file.  Needs to be wav and in the specific encoding required by CMUSphinx.");
		al.addArgument("transcript").help("An SWC XML file containing pronunciation tokens of the words which are spoken in the audio.");
		al.addArgument("output").help("Where to put the generated SWC XML file, containing the timings.");
		al.addArgument("--phone").action(new StoreTrueArgumentAction()).type(Boolean.class).setDefault(false);

		Subparser tokenize = subparsers.addParser("tokenize")
				.description("This step takes wiki HTML and creates the initial SWC XML file. " +
						"The structural info from the HTML file is kept, such as sections and paragraphs. " +
						"Also in this step, certain portions of the document are wrapped in an 'ignore' tag " +
						"to signalize that these parts are not read out in the spoken version of the article. " +
						"This includes parts to the document which are not really content, such as '[Bearbeiten]' in " +
						"the section headers, as well as whole sections which are simply not read out, such as 'Weblinks'. " +
						"This filtering requires a language to be specified.\n" +
						"The introductory text which is read for every article is also added in this step, which " +
						"also requires a language to be specified.");
		tokenize.addArgument("article_dir").help("The location of the wiki.html and info.json.");
		tokenize.addArgument("lang").choices("de", "en").help("The language used for ignoring sections, the introductory text, tokenization and normalization.");
		tokenize.addArgument("output").help("The path where the generated output file should be written to.");
		tokenize.addArgument("-i", "--no_introduction").help("Don't add the introductory text.").action(new StoreTrueArgumentAction()).setDefault(false);
		tokenize.addArgument("-a", "--all_sections").help("Don't ignore any sections.").action(new StoreTrueArgumentAction()).setDefault(false);
		tokenize.addArgument("-n", "--null_normalize").help("Disables normalization.").action(new StoreTrueArgumentAction()).setDefault(false);
		tokenize.addArgument("--raw_output").help("Also store intermediate SWC after ignoring but before flattening, tokenization or normalization under the given name");

		Subparser onlytokenize = subparsers.addParser("onlytokenize")
				.description("Takes an existing minimal SWC XML file (without any internal structure for the body, just text and tokenizes.");
		onlytokenize.addArgument("input_file").help("Path to the SWC XML input file.");
		onlytokenize.addArgument("output").help("The path where the generated output file should be written to.");
		onlytokenize.addArgument("lang").choices("de", "en").help("The language used for tokenization and normalization.");
		onlytokenize.addArgument("-n", "--null_normalize").help("Disables normalization.").action(new StoreTrueArgumentAction()).setDefault(false);
		
		Subparser ex = subparsers.addParser("export")
				.description("Various conversion operations.  Converting to formats used by other tools, " +
						"such as .lab files for Wavesurfer or the legacy .json format.");
		ex.addArgument("input_file").help("Path to the SWC XML input file.");
		ex.addArgument("--legacy_json").help("Output path of the generated legacy Json file.");
		ex.addArgument("--tokens_lab").help("Output path of the lab file containing the normalized tokens and their timings");
		ex.addArgument("--phones_lab").help("Output path of the lab file containing the maused phones and their timings");

		Subparser se = subparsers.addParser("extractsnippets"); // Subparser for snippet extraction
		se.addArgument("type").choices("training", "sentences", "kaldi").help("Type of snippets to generate");
		se.addArgument("audio").help("Path to the audio file");
		se.addArgument("align_data").help("Path to the aligned.swc for the audio");
		se.addArgument("output_dir").help("Path to the directory where snippets should be put");

		Subparser map = subparsers.addParser("mausmap").description(
				"Initial Step in MAUSing: Parse the alignment. Select words with existing timings. Create snippets (txt + wav) and jobs for these stretches.");
		map.addArgument("articles_dir").help("The root of article directories with their audio and alignment data and destination of maus alignment swc.").required(true);
		map.addArgument("-a", "--all").help("perform mausmap for all subdirectories/articles; otherwise, mausmap only one article").action(new StoreTrueArgumentAction()).type(Boolean.class).setDefault(false);
		map.addArgument("-m", "--maus").help("Path to maus installation, default: maus/maus").setDefault("maus/maus");
		map.addArgument("-g", "--g2p_wrapper").help("Path to our Sequitur g2p wrapper, default: sequitur/apply_model.sh").setDefault("sequitur/apply_model.sh");
		map.addArgument("-gm", "--g2p_model").help("Path to g2p model for use with Sequitur").required(true);
		map.addArgument("--alignment_file").help("Name of SWC XML file which stores the alignment, default: aligned.swc").setDefault("aligned.swc");
		map.addArgument("-j", "--job_dir").help("Directory where jobs should be stored, default: pending_jobs/").setDefault("pending_jobs/");
		map.addArgument("--snippet_dir").help("relative placement of intermediate snippets in article directory, default: maus").setDefault("maus");
		
		Subparser merge = subparsers.addParser("mausmerge").description(
				"Final Step in MAUSing: Read intermediary alignment, read maus'ed files, integrate maus timings."); 
		merge.addArgument("articles_dir").help("The location of input data.");
		merge.addArgument("-a", "--all").help("perform mausmerge for all subdirectories/articles; otherwise, mausmerge only one article").action(new StoreTrueArgumentAction()).type(Boolean.class).setDefault(false);
		merge.addArgument("--snippet_dir").help("relative placement of intermediate snippets in article directory").setDefault("maus");

		Subparser tt = subparsers.addParser("test"); //TODO: add description
		tt.addArgument("filename");

		try {
			Namespace ns = parser.parseArgs(args);

			switch (ns.getString("subparser_name")) {
			case "tokenize":
				tokenizeMain(ns, parser);
				break;
			case "onlytokenize":
				tokenizeExistingSWC(ns, parser);
				break;
			case "align":
				alignMain(ns, parser);
				break;
			case "export":
				exportMain(ns, parser);
				break;
			case "extractsnippets":
				extractSnippetsMain(ns, parser);
				break;
			case "mausmap":
				new Maus(ns).prepare4Maus(ns);
				break;
			case "mausmerge":
				new Maus(ns).merge();
				break;
			case "test":
				testMain(ns, parser);
				break;
			}
		} catch (ArgumentParserException e) {
			parser.handleError(e);
			exit(1);
		}
	}

	/** TODO: what does this do? */
	private static void testMain(Namespace ns, ArgumentParser parser) throws JDOMException, IOException {
		SAXBuilder jdomBuilder = new SAXBuilder();
		jdomBuilder.setJDOMFactory(new LocatedJDOMFactory());
		org.jdom2.Document doc = jdomBuilder.build(new File(ns.getString("filename")));

		LocatedElement elem = (LocatedElement) doc.getRootElement();

		for (Content x : elem.getDescendants()) {
			Content.CType type = x.getCType();
			if (type == Content.CType.Text) {
				LocatedText txt = (LocatedText) x;
				System.out.println(String.format("(%d, %d): '%s'", txt.getLine(), txt.getColumn(), txt.getText()));
			}
		}
	}


	private static void tokenizeMain(Namespace ns, ArgumentParser parser) throws Exception {
		// ### Prepare arguments ###########
		File articleDir = new File(ns.getString("article_dir"));
		String langStr = ns.getString("lang");
		Locale lang = toLocale(langStr);
		if (lang == null)
			throw new ArgumentParserException("lang not valid", parser);
		boolean addPreamble = !ns.getBoolean("no_introduction");
		boolean ignoreSections = !ns.getBoolean("all_sections");
		boolean normalize = !ns.getBoolean("null_normalize");
		String outputFilename = ns.getString("output");
		String transcriptFilename = ns.getString("raw_output");

		// ### Initialize Document #########
		DocumentInitializer di = new DocumentInitializer(articleDir);
		try {
			di.addConstants();
			di.readJsonInfo();
		} catch (Exception e) {
			//e.printStackTrace();
			logger.warn("could not extract all of info.json: ");
			di = new DocumentInitializer(articleDir);
		}
		org.jdom2.Document doc = di.getInitialDocument();
		// Add information for this processing step
		new SwcXmlUtil(doc).addProcessingStepInfo("tokenize", ns.toString());

		// ### Extract HTML Information ####
		String text = new String(Files.readAllBytes(new File(articleDir, "wiki.html").toPath()), StandardCharsets.UTF_8);
		String wikiHtml = "<body>" + text + "</body>";
		org.jdom2.Document wikiHtmlDoc = null;
		try (InputStream inStream = IOUtils.toInputStream(wikiHtml)) {
			SAXBuilder jdomBuilder = new SAXBuilder();
			jdomBuilder.setJDOMFactory(new LocatedJDOMFactory());
			wikiHtmlDoc = jdomBuilder.build(inStream);
		} catch (JDOMException e) {
			throw new Exception("wiki.html has invalid xml structure", e);
		}
		TranscriptExtractor te = new TranscriptExtractor(doc, wikiHtmlDoc);
		logger.info("Extracting Transcript");
		te.extractDocument();
		if (addPreamble) {
			logger.info("Adding Preamble");
			te.addPreamble(langStr);
		}
		if (ignoreSections) {
			logger.info("Adding ignore tag to blacklisted sections");
			te.ignoreSections(langStr);
		}

		if (transcriptFilename != null) {
			logger.info("Writing transcript to intermediate file.");
			try (OutputStream out = new FileOutputStream(new File(transcriptFilename))) {
				new XMLOutputter(rawXmlFormat).output(doc, out);
			} catch (IOException e) {
				throw new ArgumentParserException("Could not write intermediate file " + transcriptFilename, e, parser);
			}
		}
		
		// get rid of ignore nodes in the DOM that are nested (by raising their children) or empty (by deleting them)
		te.flattenIgnores();

		// ### Flatten Text Nodes ##########
		// pseudo-serialization to merge adjacent text nodes
		logger.info("Serializing document internally.");
		StringWriter writer = new StringWriter();
		new XMLOutputter(rawXmlFormat).output(doc, writer);
		StringReader reader = new StringReader(writer.toString());
		doc = new SAXBuilder().build(reader);

		// ### Tokenize with Mary ##########
		logger.info("Tokenizing.");
		try {
			MaryTokenization mt = new MaryTokenization(lang, normalize);
			mt.tokenize(doc); // modifies the document in place
		} catch (MaryConfigurationException e) {
			throw new ArgumentParserException("Problem configuring MaryTTS", e, parser);
		}

		// ### make CDATA equivalent again #
		logger.info("Postprocessing extra tag");
		SwcXmlUtil sxu = new SwcXmlUtil(doc);
		sxu.getExtraTags().forEach(SwcXmlUtil::moveCdataToAttribute);

		logger.info("Writing document to output file.");
		try (OutputStream out = new FileOutputStream(new File(outputFilename))) {
			new XMLOutputter(rawXmlFormat).output(doc, out);
		} catch (IOException e) {
			throw new ArgumentParserException("Could not write output file " + outputFilename, e, parser);
		}
	}
	
	private static void tokenizeExistingSWC(Namespace ns, ArgumentParser parser) throws ArgumentParserException {
		org.jdom2.Document doc = readDocumentFromFile(ns.getString("input_file"), parser);
		String langStr = ns.getString("lang");
		Locale lang = toLocale(langStr);
		if (lang == null)
			throw new ArgumentParserException("lang not valid", parser);
		boolean normalize = !ns.getBoolean("null_normalize");
		String outputFilename = ns.getString("output");
		// ### Tokenize with Mary ##########
		logger.info("Tokenizing.");
		try {
			MaryTokenization mt = new MaryTokenization(lang, normalize);
			mt.tokenize(doc); // modifies the document in place
		} catch (MaryConfigurationException e) {
			throw new ArgumentParserException("Problem configuring MaryTTS", e, parser);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ArgumentParserException("Problem processing with maryTTS", e, parser);
		}

		// ### make CDATA equivalent again #
		logger.info("Postprocessing extra tag");
		SwcXmlUtil sxu = new SwcXmlUtil(doc);
		sxu.getExtraTags().forEach(SwcXmlUtil::moveCdataToAttribute);

		logger.info("Writing document to output file.");
		try (OutputStream out = new FileOutputStream(new File(outputFilename))) {
			new XMLOutputter(rawXmlFormat).output(doc, out);
		} catch (IOException e) {
			throw new ArgumentParserException("Could not write output file " + outputFilename, e, parser);
		}
	}

	/**
	 * Takes SWC XML containing tokens and adds timings to each token.
	 */
	private static void alignMain(Namespace ns, ArgumentParser parser) throws ArgumentParserException, IOException {
		// *** Prepare arguments ***
		URL audioUrl;
		try {
			audioUrl = toUrl(ns.getString("audio"));
		} catch (MalformedURLException e) {
			throw new ArgumentParserException("Could not convert Audio filename to URI", e, parser);
		}
		org.jdom2.Document docWithPronunciation = readDocumentFromFile(ns.getString("transcript"), parser);

		// Add information for this processing step
		new SwcXmlUtil(docWithPronunciation).addProcessingStepInfo("align", ns.toString());

		logger.info("Aligning: Audiofile, transcript loaded");

		debugPrintMaryConfig();

		// *** Do the stuff ****
		try {
			SpeechAligner aligner = new DefaultSpeechAligner(ns.getString("acoustic_model"), ns.getString("dict"),
					ns.getString("g2p"), ns.getBoolean("phone"));

			aligner.addAlignment(docWithPronunciation, audioUrl);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ArgumentParserException("A problem occured while aligning", e, parser);
		}

		logger.info("Writing transformed document to output file.");
		try (OutputStream out = new FileOutputStream(new File(ns.getString("output")))) {
			new XMLOutputter(rawXmlFormat).output(docWithPronunciation, out);
		} catch (IOException e) {
			throw new ArgumentParserException("Could not write output file " + ns.getString("output"), e, parser);
		}
	}

	private static void extractSnippetsKaldi(Namespace ns, ArgumentParser parser) {
		File alignDirs = new File(ns.getString("align_data"));
		if (! alignDirs.isDirectory()) {
			System.err.println("the align_data directory does not exist!");
			exit(1);
		}
		HashMap<String, Integer> speakerMap = new HashMap<>();
		int articleId = 0;
		for (File subdir: alignDirs.listFiles()) {
			if (! subdir.isDirectory())
				continue;
			try {
				File audioFile = toExistingFile(subdir.getAbsolutePath() + "/audio.wav");
				File outputDir = toDirectory(ns.getString("output_dir"));
				org.jdom2.Document document = readDocumentFromFile(subdir.getAbsolutePath() + "/aligned.swc", parser);
				SnippetExtractor se = new KaldiSnippetExtractor(audioFile, document, outputDir, subdir.getName(), speakerMap, articleId);
				articleId += 1;
				se.generateTrainingSnippets();
			} catch (IOException | UnsupportedAudioFileException | ArgumentParserException e) {
				System.err.println("could not process " + subdir.getName());
			}
		}
	}

	private static void extractSnippetsMain(Namespace ns, ArgumentParser parser) throws ArgumentParserException {
		if (ns.getString("type").equals("kaldi")) {
			extractSnippetsKaldi(ns, parser);
			return;
		}
		File audioFile;
		File outputDir;
		org.jdom2.Document document;
		try {
			audioFile = toExistingFile(ns.getString("audio"));
			outputDir = toDirectory(ns.getString("output_dir"));
		} catch (ArgumentParserException e) {
			throw new ArgumentParserException(e, parser);
		}

		document = readDocumentFromFile(ns.getString("align_data"), parser);
		try {
			SnippetExtractor se = new SnippetExtractor(audioFile, document, outputDir);
			if (ns.getString("type").equals("training"))
				se.generateTrainingSnippets();
			else if (ns.getString("type").equals("sentences"))
				se.generateSentenceSnippets();
		} catch (IOException | UnsupportedAudioFileException e) {
			throw new ArgumentParserException(e, parser);
		}
	}
	
	// **************** Utility methods ********************

	private static void exportMain(Namespace ns, ArgumentParser parser) throws ArgumentParserException {
		org.jdom2.Document doc = readDocumentFromFile(ns.getString("input_file"), parser);
	
		if (ns.getString("legacy_json") != null) {
			try (OutputStream out = new FileOutputStream(new File(ns.getString("legacy_json")))) {
				DataIO.writeLegacyJsonFile(doc, out);
			} catch (IOException e) {
				throw new ArgumentParserException(e, parser);
			}
		}
		if (ns.getString("tokens_lab") != null) {
			try (OutputStream out = new FileOutputStream(new File(ns.getString("tokens_lab")))) {
				DataIO.writeNormalizedTokensLabFile(doc, out);
			} catch (IOException e) {
				throw new ArgumentParserException(e, parser);
			}
		}
		if (ns.getString("phones_lab") != null) {
			try (OutputStream out = new FileOutputStream(new File(ns.getString("phones_lab")))) {
				DataIO.writeNormalizedPhonesLabFile(doc, out);
			} catch (IOException e) {
				throw new ArgumentParserException(e, parser);
			}
		}
	}

	private static Locale toLocale(String lang) {
		switch (lang) {
		case "de":
			return Locale.GERMAN;
		case "en":
			return new Locale("en", "US"); // Region needs to be specified here,
											// GB would also work (supposedly)
		default:
			return null;
		}
	}

	/**
	 * Converts a filename as a string to a URL. Used to convert audio file
	 * names to URLs, because Sphinx takes URLs
	 * 
	 * @param filename
	 *            the Filename as a String
	 * @return the Filename as a URL
	 * @throws MalformedURLException
	 *             if something with conversion fails
	 */
	private static URL toUrl(String filename) throws MalformedURLException {
		return new File(filename).toURI().toURL();
	}

	/**
	 * Converts a filename to a File, also ensures that the file is a directory
	 * and exists.
	 */
	private static File toDirectory(String dirname) throws ArgumentParserException {
		File file = new File(dirname);
		if (!file.exists()) {
			boolean created = file.mkdirs();
			if (!created) {
				throw new ArgumentParserException(String.format("Could not create directory '%s'", dirname), null);
			}
		}

		if (!file.isDirectory()) {
			throw new ArgumentParserException(
					String.format("The given filename '%s' already exists but is not a directory", dirname), null);
		}

		// file exists and is a directory
		return file;
	}

	private static File toExistingFile(String filename) throws ArgumentParserException {
		File file = new File(filename);
		if (!file.exists())
			throw new ArgumentParserException(String.format("The file '%s' does not exist", filename), null);
		if (!file.isFile())
			throw new ArgumentParserException(String.format("The file '%s' is not a file", filename), null);
		return file;
	}


	private static void debugPrintMaryConfig() {
		for (MaryConfig conf : MaryConfig.getLanguageConfigs()) {
			logger.debug(String.format("Mary Config in Loader: %s", conf));
			LanguageConfig lc = (LanguageConfig) conf;
			Set<Locale> locales = lc.getLocales();
			for (Locale l : locales) {
				logger.debug(String.format("Mary Config Locales: %s", l));
			}
		}
	}


	/**
	 * Convenience method wrapping exception handling for file reading and parsing
	 * @param filename  The filename containing the SWC XML.
	 * @param parser  The parser.  Used for throwing exceptions
	 * @return  the jdom2 document if it can be read.
	 * @throws ArgumentParserException  for FileNotFoundEx., IOEx., JDOMEx
	 */
	private static org.jdom2.Document readDocumentFromFile(String filename, ArgumentParser parser) throws ArgumentParserException {
		try (InputStream in = new FileInputStream(new File(filename))) {
			return new SAXBuilder().build(in);
		} catch (FileNotFoundException e) {
			String msg = String.format("The File %s was not found", filename);
			throw new ArgumentParserException(msg, e, parser);
		} catch (IOException e) {
			String msg = String.format("Something went wrong while accessing the file %s", filename);
			throw new ArgumentParserException(msg, e, parser);
		} catch (JDOMException e) {
			String msg = String.format("An error occured while parsing the XML content of the file %s", filename);
			throw new ArgumentParserException(msg, e, parser);
		}
	}

}
