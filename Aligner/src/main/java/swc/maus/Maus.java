package swc.maus;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities;
import org.jsoup.parser.Parser;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;

import net.sourceforge.argparse4j.inf.Namespace;
import swc.data.SwcXmlUtil;

/**
 * Add phone-by-phone alignments to Sphinx robust word-by-word alignment via MAUS
 * @author marcel, timo
 */

public class Maus {

	private static String audioFilename = "audio.wav";
	private static String mapFilename = "align_maus_map.swc";
	private static String mergeFilename = "align_maus.swc";

	private static String COUNTER_ATTR = "mausID";
	private static String CUT_OFFSET_ATTR = "cutOffset";

	public static int versatz = 300; // TODO this is still a little dubious

	private final File articlesDir;
	private final boolean articlesInSubDirs; 
	private String snippetDir;
	private String alignmentFile;
	private File jobDir;
	
	private String g2pWrapper;
	private String g2pModel;
	private String mausCmd;
	
	private static String SENTENCE_LIKES = SwcXmlUtil.SENTENCE_TAG + "," + SwcXmlUtil.SECTIONTITLE_TAG;
	private static String MAUSED_SENTENCE_LIKES = SwcXmlUtil.SENTENCE_TAG+"["+COUNTER_ATTR+"],"+SwcXmlUtil.SECTIONTITLE_TAG+"["+COUNTER_ATTR+"]";
	private static String SUCCESSFULLY_MAUSED_SENTENCE_LIKES = SwcXmlUtil.SENTENCE_TAG+":has("+SwcXmlUtil.PHONE_TAG+"),"+SwcXmlUtil.SECTIONTITLE_TAG+":has("+SwcXmlUtil.PHONE_TAG+")";
	private static String NOT_IGNORED_PRONS = SwcXmlUtil.PRONUNCIATION_TAG+":not("+SwcXmlUtil.IGNORE_TAG+" "+SwcXmlUtil.PRONUNCIATION_TAG+")";
	private static String FULLY_ALIGNED_PRONS = SwcXmlUtil.PRONUNCIATION_TAG+"["+SwcXmlUtil.PRONUNCIATION_ATTR+"]["+SwcXmlUtil.SPEECH_START_ATTR+"]["+SwcXmlUtil.SPEECH_END_ATTR+"]";
	
	/** get some settings that are used in both map and merge from argument namespace */
	public Maus(Namespace ns) throws IOException {
		System.err.println(ns.toString());
		articlesDir = new File(ns.getString("articles_dir")).getAbsoluteFile().getCanonicalFile();
		articlesInSubDirs = ns.getBoolean("all");
		snippetDir = ns.getString("snippet_dir");
	}

	/**
	 * Liest Alignment, prüft welche Sätze von Maus aligniert werden können und
	 * generiert für diese entsprechende Job-Scripts
	 */
	public void prepare4Maus(Namespace ns) throws IOException {
		jobDir = new File(ns.getString("job_dir")).getAbsoluteFile().getCanonicalFile();
		g2pWrapper = ns.getString("g2p_wrapper");
		g2pModel = ns.getString("g2p_model");
		mausCmd = ns.getString("maus");
		alignmentFile = ns.getString("alignment_file");
		if (!jobDir.exists())
			jobDir.mkdir();
		if (articlesInSubDirs) {
			for (File article : articlesDir.listFiles(new FileFilter(){public boolean accept(File pathname) {return pathname.isDirectory();}})) {
				try {
					prepare4MausArticle(article.getAbsolutePath());
				} catch (Exception e) {
					System.out.println("INFO1: Unbekannter Fehler, Datei: " + article);
					e.printStackTrace();
				}
			}
		} else {
			prepare4MausArticle(articlesDir.getAbsolutePath());
		}
		System.out.println("Please run the jobs now to have all files maused.");
	}
	
	private void prepare4MausArticle(String articleDir) throws IOException {
		File articleXML = new File(articleDir + File.separator + alignmentFile);
		Document doc = Jsoup.parse(new FileInputStream(articleXML), "UTF-8", "", Parser.xmlParser());
		markAlignableSentences(doc);
		writeDocument(doc, articleDir + File.separator + mapFilename); 
		processSentences(doc, articleDir);
	}

	/**
	 * Sucht im Alignment nach Sätzen deren Alignment vollständig ist und
	 * versieht diese mit einem counter Attribut
	 */
	private static void markAlignableSentences(Document doc) {
		int sufficientlyAligned = 0;
		int allSentenceLikes = 0;
		for (Element sentenceLike : doc.select(SENTENCE_LIKES)) { 
			//System.err.println(satz);
			if (normWordsSufficientlyAligned(sentenceLike)) {
				Elements woerterimsatz = sentenceLike.select(SwcXmlUtil.PRONUNCIATION_TAG+"["+SwcXmlUtil.PRONUNCIATION_ATTR+"]");
				sentenceLike.attr(COUNTER_ATTR, String.format("%05d", sufficientlyAligned));
				sentenceLike.attr(CUT_OFFSET_ATTR, ermittelStartwertSatz(woerterimsatz));
				++sufficientlyAligned;
			}
			++allSentenceLikes;
		}
		double prop = ((double) sufficientlyAligned) / ((double) allSentenceLikes);
		System.out.println("INFO1: Für Maus ausreichend alignierte Sätze: " + prop);
	}

	/**
	 * checks whether all (but one) n-tags in the sentencelike were aligned. If so, we're good to go.
	 * also ensures that at least one aligned n-tag is present
	 * TODO: extend to allow multiple missing words as long as they are not consecutive
	 * TODO: AND (further limiting condition) as long as their pronunciation is (very) similar to original text 
	 */
	private static boolean normWordsSufficientlyAligned(Element sentenceLike) {
		Elements notIgnored = sentenceLike.select(NOT_IGNORED_PRONS); // these elements are n-tags
		int aligned = 0;
		int unnormalNotAligned = 0;
		int consecutiveNormalNonAligned = 0;
		int maxConsecutiveNormalNonAligned = 0;
		for (Element el : notIgnored) { // we iterate over the elements because CSS selectors stink: they don't return duplicate nodes multiple times (like <t>,</t>) but just once
			if (!el.select(FULLY_ALIGNED_PRONS).isEmpty()) {
				aligned++;
				consecutiveNormalNonAligned = 0;
			} else if (!el.select(SwcXmlUtil.PRONUNCIATION_TAG+":not(:has("+FULLY_ALIGNED_PRONS+"))").isEmpty()) {
				if (el.parent().text().equals(el.select(SwcXmlUtil.PRONUNCIATION_TAG).get(0).attr(SwcXmlUtil.PRONUNCIATION_ATTR))) {
					consecutiveNormalNonAligned++;
					maxConsecutiveNormalNonAligned = Math.max(maxConsecutiveNormalNonAligned, consecutiveNormalNonAligned);
					//System.err.println("consecutiveNormalNonAligned now at " + consecutiveNormalNonAligned);
				} else {
					unnormalNotAligned++;
				}
			} else {
				System.err.println("oups: " + el);
			}
		}
		int allWords = notIgnored.size(); 
		//assert aligned + unnormalNotAligned == allWords;
		boolean mostWordsAligned = aligned > 0 
								&& maxConsecutiveNormalNonAligned < 4
								&& unnormalNotAligned < 1 
								&& notIgnored.first().hasAttr(SwcXmlUtil.SPEECH_START_ATTR) 
								&& notIgnored.last().hasAttr(SwcXmlUtil.SPEECH_END_ATTR);
/*		if (!mostWordsAligned) {
			System.err.println("not aligning because ...");
			if (aligned == 0) System.err.println("... no aligned words in sentence-like"); else {
				if (!notIgnored.first().hasAttr(SwcXmlUtil.SPEECH_START_ATTR)) System.err.println("... first word without start time");
				if (!notIgnored.last().hasAttr(SwcXmlUtil.SPEECH_END_ATTR)) System.err.println("... last word without end time");
				if (unnormalNotAligned > 1) 
					System.err.println("... too many unnormal words not aligned");
				if (maxConsecutiveNormalNonAligned > 1) 
					System.err.println("... too many normal words in a row not aligned");
				System.err.println(sentenceLike.outerHtml());
			}
		}*/
		return mostWordsAligned;
	}

	/** print out the document that we've read */
	private static void writeDocument(Document doc, String destination)
			throws UnsupportedEncodingException, FileNotFoundException, IOException {
		Entities.EscapeMode em = Entities.EscapeMode.xhtml;
		em.getMap().clear();
		doc.outputSettings().indentAmount(0).prettyPrint(false).escapeMode(em).charset(StandardCharsets.UTF_8);
		BufferedWriter writer = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(new File(destination)), "UTF-8"));
		writer.write(doc.toString().replaceAll("&#xa0;", "\u00A0"));
		writer.close();
	}

	/**
	 * extracts the content of sentences that we're able to maus and for every sentence...
	 */
	private void processSentences(Document doc, String articleDir) throws IOException, FileNotFoundException {
		ensureSnippetDir(articleDir);
		Elements sentenceLikes = doc.select(MAUSED_SENTENCE_LIKES);
		for (Element sentenceLike : sentenceLikes) {
			String id = sentenceLike.attr(COUNTER_ATTR);
			Elements notIgnored = sentenceLike.select(NOT_IGNORED_PRONS);
			processOneSentence(id, doc, notIgnored, articleDir);
		}
	}
	
	private void ensureSnippetDir(String articleDir) {
		File snippetDir = new File(articleDir + File.separator + this.snippetDir);
		if (!snippetDir.exists())
			snippetDir.mkdir();
	}
	
	/**
  	 *   * produces a .txt for every sentence (with 1 word per line)
	 *   * produces a .job for every sentence (which will later result in cuttig of audio, G2P on .txt and then maus  
	 */
	private void processOneSentence(String id, Document doc, Elements notIgnored, String articleDir) throws IOException, FileNotFoundException {
		writeText(articleDir, notIgnored, id);
		String audiocommand = cutAudioCommand(articleDir, ermittelStartwertSatz(notIgnored), ermittelStopwertSatz(doc, notIgnored), id);
		writeJobScript(id, articleDir, audiocommand);
	}
	
	/**
	 * Schreibt Text des Satzes in eine Textdatei (eine Zeile je Wort)
	 */
	private void writeText(String articleDir, Elements notIgnored, String id) throws IOException {
		File file = new File(articleDir + File.separator + snippetDir + File.separator + id + ".txt");
		PrintWriter writer = new PrintWriter(file); //, "ISO-8859-15"); --> it turns out that our sequitur wrapper deals with the encoding
		for (Element pron : notIgnored) {
			writer.println(pron.attr(SwcXmlUtil.PRONUNCIATION_ATTR));
		}
		writer.close();
	}

	/**
	 * Mit Sox entsprechendes Audiofragment aus	der Artikelaudio schneiden
	 */
	private String cutAudioCommand(String articleDir, String start, String stop, String counter) {
		String audioOut = articleDir + File.separator + snippetDir + File.separator + counter + ".wav";
		String audioIn = articleDir + File.separator + audioFilename;
		double startT = Double.parseDouble(start) / 1000.0; // Millisekunden ->
															// Sekunden
		double stopT = Double.parseDouble(stop) / 1000.0;
		return "sox \"" + audioIn + "\" \"" + audioOut + "\" trim " + startT + " =" + stopT + "\n";
	}

	/**
	 * needs documentation! 
	 * TODO: does this actually do what we discussed?
	 * returns time in milliseconds
	 */
	private static String ermittelStartwertSatz(Elements woerterimsatz) {
		String startTime = !"".equals(woerterimsatz.first().attr(SwcXmlUtil.SPEECH_START_ATTR)) ? 
							woerterimsatz.first().attr(SwcXmlUtil.SPEECH_START_ATTR) :
							woerterimsatz.get(1).attr(SwcXmlUtil.SPEECH_START_ATTR);
		return Math.max(Integer.parseInt(startTime) - versatz, 0) + "";
	}

	/**
	 * needs documentation! 
	 * TODO: does this actually do what we discussed?
	 * returns time in milliseconds
	 */
	private static String ermittelStopwertSatz(Document doc, Elements woerterimsatz) {
		return Math.min(Integer.parseInt(woerterimsatz.last().attr(SwcXmlUtil.SPEECH_END_ATTR)) + versatz,
				Integer.parseInt(doc.select(SwcXmlUtil.PRONUNCIATION_TAG+"["+SwcXmlUtil.SPEECH_START_ATTR+"]["+SwcXmlUtil.SPEECH_END_ATTR+"]").last().attr(SwcXmlUtil.SPEECH_END_ATTR))) + "";
	}

	/**
	 * Erstellt ein Jobscript für den Satz, welches Audio schneidet sowie g2p und Maus ausführt
	 */
	private void writeJobScript(String counter, String articleDir, String audiocommand) throws FileNotFoundException {
		// job_dir/articleName/counter.sh
		String job = jobDir.getAbsolutePath() + File.separator + "maus_" + new File(articleDir).getName() + "." + counter + ".sh";
		PrintWriter writer = new PrintWriter(job);
		writer.print("#!/bin/sh\n");
		//writer.print("#Audiodatei schneiden\n");
		writer.print(audiocommand);
		String basename = articleDir + File.separator + snippetDir + File.separator + counter;
		//writer.print("#Phoneme erstellen (als txt)\n");
		writer.print(g2pCommand(basename));
		//writer.print("#Maus\n");
		writer.print(mausCommand(basename));
		writer.close();
	}

	/**
	 * Erstellt Command im Jobscript: g2p für Satz ausführen
	 */
	private String g2pCommand(String basename) {
		String txt = basename + ".txt";
		String g2p = basename + ".g2p";
		String par = basename + ".par";	
		return "\"" + g2pWrapper + "\" \"" + g2pModel + "\" \"" + txt + "\" > \"" + basename + ".g2p\"\n" // perform Sequitur G2P
			 + "cut -f 1 < \"" + g2p + "\" | nl -s' ' -v0 -ba -nln -w1 | sed 's/^/ORT: /' > \"" + par + "\"\n" + // reformat to PAR format (1)
			   "cut -f 2 < \"" + g2p + "\" | sed 's/ //g' | nl -s' ' -v0 -ba -nln -w1 | sed 's/^/KAN: /' >> \"" + par + "\"\n"; // reformat to PAR format (2)
	}

	/**
	 * Erstellt den Befehl zum Mausen
	 */
	private String mausCommand(String basename) {
		return mausCmd + " \"SIGNAL=" + basename + ".wav\" \"BPF=" + basename + ".par\" \"OUT=" + basename + ".maus\"\n";
	}

	
	public void merge() throws IOException {
		if (articlesInSubDirs) {
			for (File article : articlesDir.listFiles(new FileFilter(){public boolean accept(File pathname) {return pathname.isDirectory();}})) {
				mergeArticle(article.getAbsolutePath());
			}
		} else {
			mergeArticle(articlesDir.getAbsolutePath());
		}
	}
	
	/**
	 * Liest Alignment und Maus-Dateien, erstellt aus Mausdateien Tags und hängt
	 * diese an entsprechender Stelle ins Alignment ein
	 */
	public void mergeArticle(String articleDir) throws IOException {
		System.out.println("Pfad: " + articleDir);
		File inputXML = null;
		Document doc = null;
		try {
			inputXML = new File(articleDir + File.separator + mapFilename);
			doc = Jsoup.parse(new FileInputStream(inputXML), "UTF-8", "", Parser.xmlParser());
		} catch (FileNotFoundException e) {
			System.out.println("no such file: " + inputXML);
			return;
		}
		Elements mausedSentenceLikes = doc.select(MAUSED_SENTENCE_LIKES);
		for (Element sentenceLike : mausedSentenceLikes) {
			String id = sentenceLike.attr(COUNTER_ATTR);
			sentenceLike.removeAttr(COUNTER_ATTR);
			int cutOffset = Integer.parseInt(sentenceLike.attr(CUT_OFFSET_ATTR)); // in milliseconds
			sentenceLike.removeAttr(CUT_OFFSET_ATTR);
			String mausName = articleDir + File.separator + snippetDir + File.separator + id + ".maus";
			try {
				Elements notIgnored = sentenceLike.select(SwcXmlUtil.PRONUNCIATION_TAG+":not("+SwcXmlUtil.IGNORE_TAG+" "+SwcXmlUtil.PRONUNCIATION_TAG+")");//select("n[pronounciation][start][end]");
				if (notIgnored.size() > 0) { // nothing to be done on empty sentences, no need to trigger an error
					BufferedReader br = new BufferedReader(new FileReader(new File(mausName)));
					try {
						String line;
						while ((line = br.readLine()) != null) {
							// System.out.println(line);
							String[] attribute = line.split("\t");
							int index = Integer.parseInt(attribute[3]);
							if (index != -1) {
								// In den Mausdateien werden Pausen mit -1
								// gekennzeichnet
								notIgnored.get(index).appendChild(createMausTag(attribute, cutOffset));
							}
						}
					} finally {
						br.close();
					}
				}
			} catch (FileNotFoundException e) {
				System.out.println("INFO1: Could not find " + mausName);
			} catch (Exception e) {
				System.out.println("INFO1: unknown exception in file " + mausName);
				e.printStackTrace();
			}
		}
		writeDocument(doc, articleDir + File.separator + mergeFilename);
		anteilErfolgreichGemaust(doc);
	}

	/**
	 * Erstellt einen neuen Maus-Tag
	 */
	private static Element createMausTag(String[] attribute, int cutOffset) {
		Element tag = new Element(Tag.valueOf(SwcXmlUtil.PHONE_TAG), "");
		tag.attr(SwcXmlUtil.SPEECH_START_ATTR, startWertMausTag(attribute, cutOffset));
		tag.attr(SwcXmlUtil.SPEECH_END_ATTR, stopWertMausTag(attribute, cutOffset));
		tag.attr(SwcXmlUtil.PHONE_TYPE_ATTR, attribute[4]);
		return tag;
	}

	/**
	 * Berechnet den Wert des Startattributs eines Maustags
	 */
	private static String startWertMausTag(String[] attribute, int cuttOffset) {
		return Integer.parseInt(attribute[1]) / 16 + cuttOffset + ""; // MAUS times are in samples (16kHz). We divide by 16 to get time in milliseconds.
	}

	/**
	 * Berechnet den Wert des Stopattributs eines Maustags
	 */
	private static String stopWertMausTag(String[] attribute, int cutOffset) {
		return (Integer.parseInt(attribute[1]) + Integer.parseInt(attribute[2]) + 1) / 16 + cutOffset + ""; // MAUS durations end one sample before the next starts; it's more convenient to add 1 sample to have clearer timings
	}

	/**
	 * Anteil der Sätze welche Maus-Tags enthalten
	 */
	private static void anteilErfolgreichGemaust(Document doc) {
		double anzahlSaetze = doc.select(SENTENCE_LIKES).size();
		double anzahlSaetzeMitMaus = doc.select(SUCCESSFULLY_MAUSED_SENTENCE_LIKES).size();
		System.out.println("INFO1: Erfolgreich gemauste Sätze: " + anzahlSaetzeMitMaus / anzahlSaetze);
	}

}
