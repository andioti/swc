package swc.transcriptextractor;

import org.jdom2.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import swc.data.SwcXmlUtil;

import java.util.Arrays;
import java.util.List;

/**
 * The TranscriptExtractor receives an initial SWC document, which contains
 * metadata about the article, as well as the wiki html as an xml document.
 * Thus, the TranscriptExtractor has no File System interaction.
 * It extracts the document structure from the wiki html and adds it to the
 * given SWC document.
 * The preamble can be added and certain sections can be ignored.  Some parts
 * of the html file are added but wrapped in 'ignore' tags.
 */
public class TranscriptExtractor {

	private static final Logger logger = LoggerFactory.getLogger(TranscriptExtractor.class);

	private static final List<String> EN_SECTION_BLACKLIST = Arrays.asList("References", "Notes and references", "External links", "Contents");
	private static final List<String> DE_SECTION_BLACKLIST = Arrays.asList("Einzelnachweise","Literatur","Sekundärliteratur",
			"Weblinks","Nachweise", "Siehe auch", "Belege","Einzelbelege", "Quellen", "Quellen und weiterführende Informationen",
			"Anmerkungen", "Anmerkungen und Einzelnachweise", "Bibliografien","Filme", "Referenzen", "Weiterführende Informationen",
			"Fußnoten","Fachbücher","Weiterführende Literatur", "Werke", "Homepage", "Diskografie", "Studioalben", "Tonträger",
			"Verweise", "Zeitschriften", "Persönlichkeiten", "Verfilmungen", "Register", "Audiobeispiele");

	private final Document wikiHtml;
	private final SwcXmlUtil sxu;
	
	
	public TranscriptExtractor(Document initialDocument, Document wikiHtml) {
		this.wikiHtml = wikiHtml;
		sxu = new SwcXmlUtil(initialDocument);
	}


	/////////////////////////
	// Document Extraction //
	////////////////////// //

	/**
	 * Extracts basic structure from the HTML into SWC XML.  Keeps 'div' tags in 'ignored' tags
	 */
	public void extractDocument() throws Exception {
		Element root = sxu.getBodyRoot();

		for (Content c : wikiHtml.getRootElement().getContent()) {
			root = transcribe(c, root);
		}
	}


	/**
	 *
	 * @param wikiNode
	 * @param currentParent
	 * @return
	 */
	private Element transcribe(Content wikiNode, Element currentParent) {
		logger.debug(String.format("Transcribing Node: %s, %s", wikiNode.getCType(), wikiNode.getCType() == Content.CType.Element ? ((Element) wikiNode).getName() : null));
		switch (wikiNode.getCType()) {
			case CDATA:
			case Text:
				currentParent.addContent(wikiNode.clone());
				break;
			case Element:
				Element elem = (Element) wikiNode;
				// Handle attribute filters
				String className = elem.getAttributeValue("class");
				if (className != null &&
					(className.contains("mw-editsection") ||
					 className.contains("geo microformat") ||
					 className.contains("coordinates") ||
					 className.equals("hauptartikel") || // TODO language specific ...
					 className.equals("thumbcaption") ||
					 className.equals("thumbinner") ||
					 className.equals("hatnote"))) {
					currentParent = addAs(elem, SwcXmlUtil.IGNORE_TAG, currentParent);
					break;
				}
				String id = elem.getAttributeValue("id");
				if (id != null &&
					(id.startsWith("cite_ref-") ||
					 id.equals("toc"))) {
					currentParent = addAs(elem, SwcXmlUtil.IGNORE_TAG, currentParent);
					break;
				}
				String style = elem.getAttributeValue("style");
				if (style != null &&
						(style.contains("display: none"))) {
					currentParent = addAs(elem, SwcXmlUtil.IGNORE_TAG, currentParent);
					break;
				}

				switch (elem.getName()) {
					case "p":
					case "ul":
						currentParent = addAs(elem, SwcXmlUtil.PARAGRAPH_TAG, currentParent);
						break;
					case "h2":
					case "h3":
					case "h4":
					case "h5":
					case "h6":
						Element section = new Element(SwcXmlUtil.SECTION_TAG);
						section.setAttribute(SwcXmlUtil.LEVEL_ATTR, Integer.toString(getLevel(elem.getName())));
						addAs(elem, SwcXmlUtil.SECTIONTITLE_TAG, section);
						Element sectioncontent = new Element(SwcXmlUtil.SECTIONCONTENT_TAG);
						section.addContent(sectioncontent);
						findNewParent(currentParent, getLevel(sectioncontent) - 1).addContent(section);
						currentParent = sectioncontent;
						break;
					case "table":
					case "img":
						currentParent = addAs(elem, SwcXmlUtil.IGNORE_TAG, currentParent);
						break;
					default: // just ignore the tag, only recurse
						currentParent = transcribeChildren(elem, currentParent);
				}
				break;
			// Ignored CTypes: EntityRef, ProcessingInstruction, DocType, Comment
		case EntityRef:
			throw new RuntimeException("oups");
		case ProcessingInstruction:
			throw new RuntimeException("oups");
		default:
			break;
		}
		logger.debug(String.format("Node %s has %s child elements", currentParent.getName(), currentParent.getChildren().size()));
		return currentParent;
	}


	private Element transcribeChildren(Element elem, Element parent) {
		for (Content c : elem.getContent())
			parent = transcribe(c, parent);
		return parent;
	}


	private Element addAs(Element elem, String tag, Element parent) {
		Element as = new Element(tag);
		parent.addContent(as);
		Element oldParent = parent;
		parent = as;
		for (Content c : elem.getContent())
			parent = transcribe(c, parent);
		if (parent != as)
			return parent;
		else
			return oldParent;
	}


	private int getLevel(String hTag) {
		return Integer.parseInt(hTag.substring(1));
	}


	private int getLevel(Element parent) {
		logger.debug(String.format("getLevel: %s", parent));
		switch (parent.getName()) {
			case SwcXmlUtil.DOC_TAG:
				return 0;
			case SwcXmlUtil.SECTIONCONTENT_TAG:
				return Integer.parseInt(parent.getParentElement().getAttributeValue(SwcXmlUtil.LEVEL_ATTR));
			default:
				return getLevel(parent.getParentElement());
		}
	}

	/**
	 * @param currentParent  just the Element above the one that is supposed to be inserted.  Might be a div or p or anything really.
	 * @param desiredDepthOfParent If a h2 section is to be inserted, the desired depth of the parent is 1
	 * @return a parent element where Elements can be inserted. Either a sectioncontent or the root node.
	 */
	private Element findNewParent(Element currentParent, int desiredDepthOfParent) {
		while (!(getLevel(currentParent) <= desiredDepthOfParent)) {
			currentParent = goUp(currentParent);
		}
		return currentParent;
	}


	private Element goUp(Element child) {
		Element parent = child.getParentElement();
		while (!(SwcXmlUtil.SECTIONCONTENT_TAG.equals(parent.getName()) || SwcXmlUtil.DOC_TAG.equals(parent.getName()))) {
			parent = parent.getParentElement();
		}
		// parent is now "sectioncontent" element
		return parent;
	}


	/**
	 * Inserts a new node with the preamble into the document
	 * @param lang either "de" or "en"
	 */
	public void addPreamble(String lang) {
		String articleTitle = sxu.getArticleTitle();
		StringBuilder result = new StringBuilder();
		switch (lang) {
			case "en":
				result.append(articleTitle);
				result.append(", from wikipedia, the free encyclopedia at e n dot wikipedia dot org.");
				break;
			case "de":
				result.append("Sie hören den Artikel ");
				result.append(articleTitle);
				result.append(", aus Wikipedia, der freien Enzyklopädie. \n");
				result.append("Mit dem Stand vom \n");
				result.append("Der Inhalt steht unter der Lizenz Creative Commons Attribution Share Alike 3 Punkt 0 Unported und unter der GNU Lizenz für freie Dokumentation.");
				break;
			default:
				throw new IllegalArgumentException(String.format("Unsupported Language: %s", lang));
		}
		sxu.addPreambleTag(result.toString());
	}


	/**
	 * Adds ignore tags to the sections which are in the blacklist for the given language.
	 * @param lang The language of the article and as such the corresponding blacklist of section titles.
	 */
	public void ignoreSections(String lang) {
		assert lang != null;

		List<String> blacklistedTitles;
		switch (lang) {
			case "en":
				blacklistedTitles = EN_SECTION_BLACKLIST;
				break;
			case "de":
				blacklistedTitles = DE_SECTION_BLACKLIST;
				break;
			default:
				throw new IllegalArgumentException(String.format("Unsupported Language: %s", lang));
		}

		List<Element> sections = sxu.getSections(SwcXmlUtil.SECTION_FIRST_LEVEL);

		for (Element section : sections) {
			String title = sxu.getSectionTitle(section);
			boolean sectionBlacklisted = blacklistedTitles.stream().anyMatch(s -> title.startsWith(s));
			if (sectionBlacklisted)
				sxu.ignoreNode(section);
		}
	}
	
	public void flattenIgnores() {
		sxu.flattenIgnores();
	}
}
