package swc.aligner.api;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;

import edu.cmu.sphinx.linguist.acoustic.Unit;
import edu.cmu.sphinx.linguist.dictionary.Word;
import edu.cmu.sphinx.result.Result;
import edu.cmu.sphinx.result.WordResult;
import edu.cmu.sphinx.util.TimeFrame;
import swc.aligner.alignment.PhoneAligner;
import swc.aligner.alignment.PhoneAligner.ScoreEntry;

public class PhoneSpeechAligner extends SpeechAligner
{
    String amPath;
    HashMap<String, Integer> phone_ids;
    float[][] relatedness_matrix;

    public PhoneSpeechAligner(String amPath, String dictPath, String g2pPath) throws MalformedURLException, IOException
    {
        super(amPath, dictPath, g2pPath);
        this.amPath = amPath;
        phone_ids = getPhoneset();
        relatedness_matrix = LoadRealtednessMatrix(phone_ids, new File(amPath, "phone_relatedness"));
    }

    @Override
    protected void onContextCreated()
    {
        context.setLocalProperty("activeList->absoluteBeamWidth", 350);
        context.setLocalProperty("decoder->searchManager", "allphoneSearchManager");
        context.setLocalProperty("allphoneLinguist->useContextDependentPhones", true);
        super.onContextCreated();
    }

    private static class WordInfo
    {
        public WordInfo(Word word)
        {
            this.word = word;
        }

        public int num_phones_found;
        public long start_time = Long.MAX_VALUE;
        public long end_time = Long.MIN_VALUE;
        public long duration_found = 0;
        public Word word;
        public int phones_start = Integer.MAX_VALUE;
        public int phones_end = Integer.MIN_VALUE;

        public void updateTime(TimeFrame part)
        {
            if (part.getEnd() > end_time)
                end_time = part.getEnd();
            if (part.getStart() < start_time)
                start_time = part.getStart();
        }
    }

    @Override
    public List<WordResult> align(URL audioUrl, List<String> transcript) throws Exception
    {
        return align(audioUrl, TimeFrame.INFINITE, transcript);
    }

    public List<WordResult> align(URL audioUrl, TimeFrame timeFrame, List<String> transcript) throws Exception
    {

        edu.cmu.sphinx.linguist.dictionary.Dictionary dictionary = context
                .getInstance(edu.cmu.sphinx.linguist.dictionary.Dictionary.class);
        dictionary.allocate();
        String silence_phone = dictionary.getSilenceWord().getMostLikelyPronunciation().getUnits()[0].getName();
        Word silence = new Word(silence_phone, dictionary.getSilenceWord().getPronunciations(), true);

        // create list of expected phones
        List<Integer> word_of_phone = new ArrayList<>();
        List<WordInfo> words = new ArrayList<>(transcript.size());
        List<String> expected_phones = new ArrayList<String>();
        expected_phones.add(silence_phone);
        word_of_phone.add(null);
        for (String word_string : transcript)
        {
            if (word_string == null || word_string.length() == 0)
                continue;
            Word word = dictionary.getWord(word_string);
            words.add(new WordInfo(word));
            Unit[] units = word.getMostLikelyPronunciation().getUnits();

            for (Unit u : units)
            {
                expected_phones.add(u.getName());
                word_of_phone.add(words.size() - 1);
            }

            expected_phones.add(silence_phone);
            word_of_phone.add(null);
        }

        // Do allphone recognition
        List<WordResult> recognized_phones = new ArrayList<WordResult>();
        context.setSpeechSource(audioUrl.openStream(), timeFrame);
        recognizer.allocate();
        Result result;
        long end_time = timeFrame.getStart();
        while ((result = recognizer.recognize()) != null)
        {
            for (WordResult r : result.getTimedBestResult(true))
            {
                // make silence explicit
                long start_time = r.getTimeFrame().getStart();
                if (start_time > end_time + 20)
                    recognized_phones.add(new WordResult(silence, new TimeFrame(end_time, start_time), -1, -1));
                end_time = r.getTimeFrame().getEnd();

                // add to results:
                recognized_phones.add(r);
            }
        }
        recognizer.deallocate();

        List<String> recognized_phone_names = new ArrayList<String>(recognized_phones.size());
        for (WordResult r : recognized_phones)
            recognized_phone_names.add(r.getWord().getSpelling());

        logger.info("Aligning " + recognized_phones.size() + " recognized phones to " + expected_phones.size()
                + " expected phones ... ");
        // align phone to phone
        // IndexPair[] matches = phone_aligner.getLongestCommonSubsequence(0,
        // expected_phones.size(), recognized_phone_names);
        if (recognized_phones.size() > 0)
        {
            List<ScoreEntry> matches = align(expected_phones, recognized_phone_names);

            for (ScoreEntry match : matches)
            {
                if (match.isMatch())
                {
                    int phone_index = match.getRight();
                    WordResult phone = recognized_phones.get(phone_index);
                    Integer word_index = word_of_phone.get(match.getLeft());
                    if (word_index == null)
                        continue;// phone does not belong to any word
                    WordInfo word = words.get(word_index);
                    word.num_phones_found++;
                    word.duration_found += phone.getTimeFrame().length();
                    word.updateTime(phone.getTimeFrame());
                    if (word.phones_start > phone_index)
                        word.phones_start = phone_index;
                    if (word.phones_end < phone_index)
                        word.phones_end = phone_index;
                }
            }
        }

        int num_words_found = 0;
        List<WordResult> words_found = new ArrayList<>();
        for (WordInfo word : words)
        {
            if (word == null)
                break;
            int num_phones = word.word.getMostLikelyPronunciation().getUnits().length;
            if (word.num_phones_found > (num_phones - 1) * 2 / 3
                    && word.duration_found + 20 > (word.end_time - word.start_time) * 0.5)
            {
                /*
                 * StringBuilder phonelist = new StringBuilder(); for(int i =
                 * word.phones_start; i <= word.phones_end; i++) { WordResult
                 * phone = recognized_phones.get(i); if(phonelist.length() > 0)
                 * phonelist.append(' ');
                 * phonelist.append(phone.getWord().getSpelling()); }
                 * 
                 * System.err.println("Pronunciation: " +
                 * word.word.getSpelling() + "\t" + phonelist);
                 */
                words_found.add(new WordResult(word.word, new TimeFrame(word.start_time, word.end_time),
                        word.num_phones_found / (float) num_phones, 0));
                num_words_found++;
            } else
            {
                words_found.add(new WordResult(word.word, null, -1, -1));
            }
        }

        // logger.info(String.format("Found %d of %d words! (%.1f %%)",
        // num_words_found, words.size(), num_words_found*100.0f/words.size()));

        return words_found;
    }

    public List<ScoreEntry> align(List<String> expected_phones, List<String> recognized_phone_names)
    {
        PhoneAligner phone_aligner = new PhoneAligner(1, 0.5f, 5, 0.4f, relatedness_matrix);
        List<ScoreEntry> matches = phone_aligner.align(
                toPhoneIds(expected_phones, phone_ids),
                toPhoneIds(recognized_phone_names, phone_ids));
        printAlignment(matches, expected_phones, recognized_phone_names);
        return matches;
    }

    private void printAlignment(List<ScoreEntry> matches, List<String> expected, List<String> recognized)
    {
        StringBuilder l1 = new StringBuilder();
        StringBuilder l2 = new StringBuilder();
        for (ScoreEntry match : matches)
        {
            String left = match.getLeft() >= 0 ? expected.get(match.getLeft()) : "";
            String right = match.getRight() >= 0 ? recognized.get(match.getRight()) : "";
            switch (match.type)
            {
            case ScoreEntry.DELETION:
                right = "-";
                break;
            case ScoreEntry.INSERTION:
                left = "-";
                break;
            case ScoreEntry.DOUBLE_MATCH_X:
                if (match.number == 2)
                    right = "\"";
                break;
            case ScoreEntry.DOUBLE_MATCH_Y:
                if (match.number == 2)
                    left = "\"";
                break;
            }
            if (left.length() > 3)
                left = left.substring(0, 3);
            if (right.length() > 3)
                right = right.substring(0, 3);

            l1.append(String.format("%1$-4s", left));
            l2.append(String.format("%1$-4s", right));
        }
        logger.log(Level.INFO, "Phone alignment:\n\t" + l1.toString() + "\n\t" + l2.toString());
    }

    public int[] toPhoneIds(List<String> phone_names, HashMap<String, Integer> phone_ids)
    {
        int[] result = new int[phone_names.size()];

        for (int i = 0; i < result.length; i++)
        {
            result[i] = phone_ids.get(phone_names.get(i));
        }
        return result;
    }

    /**
     * Parse the ffile containing the relatedness matrix
     * Two formats are supported:
     * 
     * Matrix:
     *      a   b   c
     * a    1.1 1.2 1.3
     * b    2.1 2.2 2.3
     * c    4.1 5.2 6.3
     *
     * OR
     * 
     * Penalty: 1.123
     * a b
     * b c
     * 
     * @param phone_ids
     * @param file
     * @return
     * @throws IOException
     */
    private float[][] LoadRealtednessMatrix(HashMap<String, Integer> phone_ids, File file) throws IOException
    {
        HashSet<String> warn_phones = new HashSet<String>();
        List<String> lines = java.nio.file.Files.readAllLines(file.toPath(), Charset.forName("utf-8"));
        float[][] matrix = new float[phone_ids.size()][phone_ids.size()];
        for (int i = 0; i < matrix.length; i++)
        {
            float[] row = matrix[i];
            for (int j = 0; j < row.length; j++)
            {
                row[j] = Float.POSITIVE_INFINITY;
            }
        }

        for (int i = 0; i < lines.size();)
        {
            String line = lines.get(i++);
            if (line.matches("^Penalty:\\s*[\\d\\.]+"))
            {
                float penalty = Float.parseFloat(line.replaceFirst("^Penalty:\\s*([\\d\\.]+)", "$1"));
                while (i < lines.size() && !"".equals(line = lines.get(i++)))
                {
                    String[] phones = line.split("\\s");
                    for (String phone1 : phones)
                    {
                        Integer id1 = phone_ids.get(phone1);
                        if (id1 == null)
                        {
                            warn_phones.add(phone1);
                            continue;
                        }
                        for (String phone2 : phones)
                        {
                            Integer id2 = phone_ids.get(phone2);
                            if (id2 == null)
                                continue;
                            if (matrix[id1][id2] > penalty)
                                matrix[id1][id2] = penalty;
                            if (matrix[id2][id1] > penalty)
                                matrix[id2][id1] = penalty;
                        }
                    }
                }
            }
            if(line.matches("^Matrix:"))
            {
                String[] columns = lines.get(i++).split("\t");
                while(i < lines.size() && !"".equals(line=lines.get(i++)) )
                {
                    String[] cells_in_row = line.split("\t");
                    String expected_phone = cells_in_row[0];
                    for(int j=1;j<cells_in_row.length;j++)
                    {
                        String found_phone = columns[j];
                        float score = Float.parseFloat(cells_in_row[j]);
                        matrix[phone_ids.get(expected_phone)][phone_ids.get(found_phone)] = score;
                    }
                }
                return matrix;
            }
        }

        for (String phone : warn_phones)
        {
            System.err.println("WARNING: matrix contains phone: " + phone);
        }
        for (int i = 0; i < matrix.length; i++)
        {
            if (matrix[i][i] == Float.POSITIVE_INFINITY)
            {
                for (Entry<String, Integer> e : phone_ids.entrySet())
                    if (e.getValue() == i)
                        System.err.println("WARNING: matrix is missing phone: " + e.getKey());
            }
        }

        for (boolean changed = true; changed;)
        {
            changed = false;
            for (int i = 0; i < matrix.length; i++)
            {
                float[] row_i = matrix[i];
                for (int j = 0; j < row_i.length; j++)
                {
                    // the following inequality must always be fulfilled:
                    // penalty(i --> j) + penalty(j --> k) >= penalty(i --> k)
                    float[] row_j = matrix[j];
                    for (int k = 0; k < row_j.length; k++)
                    {
                        float penalty_ij = row_i[j];
                        float penalty_ik = row_i[k];
                        float penalty_jk = row_j[k];
                        float penalty_ijk = penalty_ij + penalty_jk;
                        if (penalty_ijk < penalty_ik)
                        {
                            changed = true;
                            row_i[k] = penalty_ijk;
                        }
                    }
                }
            }
        }
        return matrix;
    }

    private HashMap<String, Integer> getPhoneset()
    {
        HashMap<String, Integer> result = new HashMap<>();
        result.put("SIL", 0);
        for (Unit u : context.getLoader().getContextIndependentUnits().values())
        {
            if (!result.containsKey(u.getName()))
                result.put(u.getName(), result.size());
        }
        return result;
    }
}
