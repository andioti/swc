package swc.aligner.alignment;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

public class PhoneAligner
{
    public static class ScoreEntry extends IndexPair implements Comparable<ScoreEntry>
    {
        public ScoreEntry(int x, int y, float delta_penalty, ScoreEntry previous, int type, int number)
        {
            super(x,y);
            this.previous = previous;
            this.delta_penalty = delta_penalty;
            if(previous != null)
            {
                this.total_penalty = previous.total_penalty + delta_penalty;
            }
            else
            {
                this.total_penalty = delta_penalty;
            }
            this.type = type;
            this.number = number;
        }
        
        public ScoreEntry previous;
        public float delta_penalty;
        public float total_penalty;
        public int type;
        public int number;
        
        @Override
        public int compareTo(ScoreEntry other)
        {
            int result = Float.compare(this.total_penalty, other.total_penalty);
            if(result != 0)
                return result;
            
            return -Integer.compare(this.getLeft()+this.getRight(), other.getLeft()+other.getRight());
        }
        
        public boolean isMatch()
        {
            return type >= MATCH && type <= DOUBLE_MATCH_Y;
        }
        
        public boolean isInsertion()
        {
            return type == INSERTION;
        }
        
        @Override
        public String toString()
        {
            return type_names[type];
        }

        private static final String[] type_names = {"NONE","MATCH","DOUBLE_MATCH_X","DOUBLE_MATCH_Y","INSERTION","DELETION"};
        public static final int NONE = 0;
        public static final int MATCH = 1;
        public static final int DOUBLE_MATCH_X = 2;
        public static final int DOUBLE_MATCH_Y = 3;
        public static final int INSERTION = 4;
        public static final int DELETION = 5;

    }
    
    private float insertion_start_penalty;
    private float insertion_penalty;
    private float deletion_start_penalty;
    private float deletion_penalty;
    private float[][] phone_matching_penalties;
    
    public PhoneAligner(float insertion_start_penalty, float insertion_penalty, float deletion_start_penalty, float deletion_penalty, float[][] phone_matching_penalties)
    {
        this.insertion_start_penalty = insertion_start_penalty;
        this.insertion_penalty = insertion_penalty;
        this.deletion_start_penalty = deletion_start_penalty;
        this.deletion_penalty = deletion_penalty;
        this.phone_matching_penalties = phone_matching_penalties;
    }
    
    private static class ScoreQueue
    {
        public ScoreQueue(int width, int height)
        {
            score_matrix = new float[width+1][height+1];
            for(int x = 0;x<=width;x++)
                for(int y = 0; y<=height; y++)
                    score_matrix[x][y] = Float.POSITIVE_INFINITY;
            this.width = width+1;
            this.height = height +1;
        }
        
        private final int width, height;
        private final float[][] score_matrix;
        private PriorityQueue<ScoreEntry> queue = new PriorityQueue<ScoreEntry>();
        private int queue_cleanup = 100000;
        
        public boolean add(ScoreEntry e)
        {
            if(e == null)
                return false;
            boolean was_inserted = false;
            float score = e.total_penalty;
            int x = e.getLeft()+1;
            int y = e.getRight()+1;
            if(score_matrix[x][y] > score)
            {
                score_matrix[x][y] = score;
                queue.add(e);
                was_inserted = true;
            }
            
            if(queue.size() >= queue_cleanup)
                cleanup();
            return was_inserted;
        }
        
        private void cleanup()
        {
            PriorityQueue<ScoreEntry> old_queue = queue;
            queue = new PriorityQueue<PhoneAligner.ScoreEntry>(old_queue.size());
            
            BitSet[] flags = new BitSet[width];//avoid multiple entries for the same field with identical scores
            for(ScoreEntry e : old_queue)
            {
                int x = e.getLeft()+1;
                int y = e.getRight()+1;
                BitSet col = flags[x];
                if(col == null)
                    flags[x] = col = new BitSet(height);
                
                if(!col.get(y) && score_matrix[x][y] == e.total_penalty)
                {
                    col.set(y);
                    queue.add(e);
                }
            }
            queue_cleanup = Math.max(1000000, queue.size()*2);
        }

        public ScoreEntry poll()
        {
            while(true)
            {
                ScoreEntry e = queue.poll();

                int x = e.getLeft()+1;
                int y = e.getRight()+1;
                if(score_matrix[x][y] == e.total_penalty)
                {
                    score_matrix[x][y] = Float.NaN;
                    return e;
                }
            }
        }
        
        public boolean already_visited(int x,int y)
        {
            return Float.isNaN(score_matrix[x+1][y+1]);
        }
    }
    
    public List<ScoreEntry> align(int[] phone_list_1, int[] phone_list_2)
    {
        if(phone_list_1.length * (long)phone_list_2.length > 900000000l)
            throw new OutOfMemoryError("lists too long");
        ScoreQueue queue = new ScoreQueue(phone_list_1.length, phone_list_2.length);
        int max_x = phone_list_1.length-1;
        int max_y = phone_list_2.length-1;
        ScoreEntry entry;
        long num_visited = 0;
        for(entry = new ScoreEntry(-1, -1, 0, null, ScoreEntry.NONE, 0); entry.getLeft() < max_x || entry.getRight() < max_y; entry = queue.poll())
        {
            num_visited++;
            int x = entry.getLeft();
            int y = entry.getRight();
            if (y < max_y && !queue.already_visited(x + 0, y + 1))
            {
                // insertion possible
                // an insertion is when an item in list2 (y coordinate) is skipped, meaning ther is no corresponsing item in list1 (x coordinate)
                queue.add(makeInsertion(x + 0, y + 1, entry, phone_list_1, phone_list_2));
            }
            if (x < max_x && y < max_y && !queue.already_visited(x + 1, y + 1))
            {
                // match possible
                // a match is when both are present
                queue.add(makeMatch(x + 1, y + 1, entry, phone_list_1, phone_list_2, 0));
                
                if (x + 1 < max_x && !queue.already_visited(x + 2, y + 1))
                {
                    float extra_penalty = 0.1f;
                    // match two elements on x axis to one on y
                    ScoreEntry first = makeMatch(x + 1, y + 1, entry, phone_list_1, phone_list_2, extra_penalty);
                    first.type = ScoreEntry.DOUBLE_MATCH_X;
                    first.number = 1;
                    ScoreEntry second = makeMatch(x + 2, y + 1, first, phone_list_1, phone_list_2, extra_penalty);
                    second.type = ScoreEntry.DOUBLE_MATCH_X;
                    second.number = 2;
                    queue.add(second);
                }
                if (x + 3 < max_x && phone_list_1[x+2] == 0 && !queue.already_visited(x + 3, y + 1))
                {
                    float extra_penalty = 0.1f;
                    // match two elements on x axis to one on y
                    ScoreEntry first = makeMatch(x + 1, y + 1, entry, phone_list_1, phone_list_2, extra_penalty);
                    first.type = ScoreEntry.DOUBLE_MATCH_X;
                    first.number = 1;
                    ScoreEntry deletion = makeDeletion(x + 2, y + 1, first, phone_list_1, phone_list_2);//delete silence
                    ScoreEntry second = makeMatch(x + 3, y + 1, deletion, phone_list_1, phone_list_2, extra_penalty);
                    second.type = ScoreEntry.DOUBLE_MATCH_X;
                    second.number = 2;
                    queue.add(second);
                }
                if (y + 1 < max_y && !queue.already_visited(x + 1, y + 2))
                {
                    float extra_penalty = 0.1f;
                    // match one element on x axis to two on y
                    ScoreEntry first = makeMatch(x + 1, y + 1, entry, phone_list_1, phone_list_2, extra_penalty);
                    first.type = ScoreEntry.DOUBLE_MATCH_Y;
                    first.number = 1;
                    ScoreEntry second = makeMatch(x + 1, y + 2, first, phone_list_1, phone_list_2, extra_penalty);
                    second.type = ScoreEntry.DOUBLE_MATCH_Y;
                    second.number = 2;
                    queue.add(second);
                }
            }
            ScoreEntry e = entry;
            while (e.getLeft() < max_x)
            {
                // deletion possible
                e = makeDeletion(e.getLeft() + 1, y + 0, e, phone_list_1, phone_list_2);
                queue.add(e);
                if (e.number >= 9)
                    break;
            }
        }
        System.err.println("visited: "+num_visited/((phone_list_1.length+1)*(double)(phone_list_2.length+1)*100.0));
        //entry is last field
        List<ScoreEntry> result = new ArrayList<PhoneAligner.ScoreEntry>();
        while(entry != null)
        {
            ScoreEntry previous = entry.previous;
            if(previous != null)
                result.add(entry);
            entry = previous;
        }
        Collections.reverse(result);
        return result;
    }

    private ScoreEntry makeMatch(int x, int y, ScoreEntry previous, int[] phone_list_1, int[] phone_list_2, float extra_penalty)
    {
        int phone1 = phone_list_1[x];
        int phone2 = phone_list_2[y];
        float penalty = phone_matching_penalties[phone1][phone2] + extra_penalty;
        return new ScoreEntry(x, y, penalty, previous, ScoreEntry.MATCH, 0);
    }

    private ScoreEntry makeInsertion(int x, int y, ScoreEntry previous, int[] phone_list_1, int[] phone_list_2)
    {
        float penalty = insertion_start_penalty;
        int num_insertions = 1;
        if(previous.type == ScoreEntry.INSERTION)
        {
            //previous was insertion
            penalty = insertion_penalty;
            num_insertions = previous.number+1;
        }
        return new ScoreEntry(x,y,penalty,previous, ScoreEntry.INSERTION, num_insertions);
    }

    private ScoreEntry makeDeletion(int x, int y, ScoreEntry previous, int[] phone_list_1, int[] phone_list_2)
    {
        float penalty = deletion_penalty;
        int num_deletions = 1;
        if(previous.type == ScoreEntry.DELETION)
        {
            //previous was deletion
            num_deletions = previous.number+1;
        }
        if(phone_list_1[x] == 0)
        {
            penalty = 0;//missing silence does not count
            num_deletions -= 1;
        }
        else
        {
            if(num_deletions == 2)
                penalty += deletion_start_penalty * 0.3f;
            if(num_deletions == 3)
                penalty = deletion_start_penalty * 0.7f;
        }
        return new ScoreEntry(x, y, penalty, previous, ScoreEntry.DELETION, num_deletions);
    }
}
