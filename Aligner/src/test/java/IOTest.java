import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.FileUtils;
import org.jdom2.JDOMException;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import swc.data.Document;
import swc.io.DataIO;


public class IOTest {

	
	@Test  // comment out do disable
	public void testReadWrite() throws JDOMException, IOException {
		// load test resource
		File exampleFile = new File(getClass().getResource("/zwieback.xml").getFile());
		// initialize temp dir where files for comparison will be written
		TemporaryFolder temp = new TemporaryFolder();
		temp.create();
		File newXmlFile = temp.newFile();
		
		// read input file, parse it and serialize it again; actual testing portion
		try (
			InputStream inFileStream = new FileInputStream(exampleFile);
			OutputStream newFileOutStream = new FileOutputStream(newXmlFile)
		) {
			Document doc = DataIO.readDocument(inFileStream); // parsing
			DataIO.writeDocumentToXml(doc, newFileOutStream); // serializing
		}
		
		assertTrue(FileUtils.contentEquals(newXmlFile, exampleFile));
	}
}
