import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.junit.Assert;
import org.junit.Test;
import swc.transcriptextractor.MaryTokenization;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Created by felix on 06/01/17.
 */
public class TranscriptExtractorTest {

    public static final String MARY_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><maryxml xmlns=\"http://mary.dfki.de/2002/MaryXML\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"0.5\" xml:lang=\"de\">\n" +
            "<p>\n" +
            "<s>\n" +
            "<t>\n" +
            "Dies\n" +
            "</t>\n" +
            "<t>\n" +
            "ist\n" +
            "</t>\n" +
            "<t>\n" +
            "ein\n" +
            "</t>\n" +
            "<t>\n" +
            "Testsatz\n" +
            "</t>\n" +
            "<t>\n" +
            ".\n" +
            "</t>\n" +
            "</s>\n" +
            "<s>\n" +
            "<t>\n" +
            "Noch\n" +
            "</t>\n" +
            "<t>\n" +
            "ein\n" +
            "</t>\n" +
            "<t>\n" +
            "weiterer\n" +
            "</t>\n" +
            "<t>\n" +
            "Satz\n" +
            "</t>\n" +
            "<t>\n" +
            ".\n" +
            "</t>\n" +
            "</s>\n" +
            "</p>\n" +
            "</maryxml>";


    @Test
    public void testMaryToSwc() throws JDOMException, IOException {
        Document maryDoc;
        try (InputStream in = new ByteArrayInputStream(MARY_XML.getBytes(StandardCharsets.UTF_8))) {
            maryDoc = new SAXBuilder().build(in);
        }
        List<Element> sentences = MaryTokenization.tranformMaryTokensToSwcTokens(maryDoc.getRootElement());
        Assert.assertEquals(2, sentences.size());
        Assert.assertEquals("s", sentences.get(0).getName());
    }


}
