* Aligner Format

** Overview

article
  metadata
  document
    paragraph
      sentence
        t
          n
        a
          t ...
    preamble
      paragraph ...
    section
      sectiontitle
      sectioncontent
        paragraph ...


The XML hierarchy consists of
- d: Document
- p: Paragraph
- s: Sentence
- t: Token
- n: normalized token or subtoken

A Document contains Paragraphs, a Paragraphs contains Sentences and a
Sentence contains Tokens.  A Token contains zero to n normalized
tokens.  Most tokens contain only one normalized token, which is
identical to the token itself.  For Tokens like "GNU", there are three
subtokens, one for each letter.  For punctuation tokens like "." there
are no subtokens because punctuation is normalized to nothing. 

** Attributes

A Token always has the "orig" attribute, containing the original
spelling of the token. 
A normalized token always has the "spelling" attribute, containing the
normalized spelling.  If the token is aligned to audio, the "start"
and "end" attributes are also set, containing the start and end time
of the token in milliseconds.

** Idea

The structure is inspired by the MaryTTS XML output, which also
contains Document, Paragraph, Sentence and Word.  However, multi token
units are handled weirdly as well as normalized enumerations like
"19." in "19. Jahrhundert". 
The way these documents are structured makes it easy to extract all
the stuff that is (supposedly) said in the audio, just by taking all
the "n" nodes in the tree.  Also if a text token isn't spoken, like
punctuation, no "n" child nodes exist. 
